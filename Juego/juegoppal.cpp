#include "juegoppal.h"
#include "ui_juegoppal.h"
#include "obstaculofisico.h"
#include "obstaculograf.h"
#include "menu.h"


JuegoPPAL::JuegoPPAL(QWidget *parent) : QMainWindow(parent),ui(new Ui::JuegoPPAL) //Constructor juego (w)
{
    //Constructor

    ui->setupUi(this);
    resultadoP=new resultados();

    player=new QMediaPlayer(this);

    Lose= new QMediaPlayer(this);
    Lose->setMedia(QUrl("qrc:/MusicaJuego/GameOver.mp3"));
    Lose->setVolume(150);

    JuegoPPLMusica=new QMediaPlayer(this);
    JuegoPPLMusica->setMedia(QUrl("qrc:/MusicaJuego/JuegoPPL.mp3"));
    JuegoPPLMusica->setVolume(30);

    rayoMusica=new QMediaPlayer(this);
    rayoMusica->setMedia(QUrl("qrc:/MusicaJuego/Rayo.mp3"));
    rayoMusica->setVolume(100);

    Rasengan=new QMediaPlayer(this);
    Rasengan->setMedia(QUrl("qrc:/MusicaJuego/rasengan.mp3"));
    Rasengan->setVolume(30);

    MonedasMusic=new QMediaPlayer(this);
    MonedasMusic->setMedia(QUrl("qrc:/MusicaJuego/Moneda.mp3"));

    VidaMusica=new QMediaPlayer(this);
    VidaMusica->setMedia(QUrl("qrc:/MusicaJuego/Vida.mp3"));

    Golpe=new QMediaPlayer(this);
    Golpe->setMedia(QUrl("qrc:/MusicaJuego/Tortuga.mp3"));
    Golpe->setVolume(100);

    explosion=new QMediaPlayer(this);
    explosion->setMedia(QUrl("qrc:/MusicaJuego/explosion.mp3"));
    clickPPAL=new QMediaPlayer(this);
    clickPPAL->setMedia(QUrl("qrc:/MusicaJuego/click.mp3"));
    clickPPAL->setVolume(100);

    clickPPAL2=new QMediaPlayer(this);
    clickPPAL2->setMedia(QUrl("qrc:/MusicaJuego/error.mp3"));
    clickPPAL2->setVolume(30);

    clickPPAL3=new QMediaPlayer(this);
    clickPPAL3->setMedia(QUrl("qrc:/MusicaJuego/correcto.mp3"));
    clickPPAL3->setVolume(60);

    win2=new QMediaPlayer(this);
    win2->setMedia(QUrl("qrc:/MusicaJuego/win2.mp3"));

    Nivel2 = new QMediaPlayer(this);
    Nivel2->setMedia(QUrl("qrc:/MusicaJuego/Nivel2.mp3"));

   nivel3 = new QMediaPlayer(this);
   nivel3->setMedia(QUrl("qrc:/MusicaJuego/nivel 3.mp3"));

    //Mapa
    QPixmap fondo;
    fondo.load(":/ImagenesJuego/Fondo/fondo1.png");

    //crear la escena
    scene=new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);                      //Pantalla principal ponga la escena (qgraph scene)
    ui->graphicsView->setBackgroundBrush(QBrush(fondo));   //Asigna el fondo
    scene->setSceneRect(24,0,1000,496);                    //Valores, posicion, x, y, tamaño x, y
    ui->graphicsView->scale(1,-1);                       //Invertir eje y

    //Inicializacion de los timers
    timer=new QTimer(this);
    timer->stop();
    timer2=new QTimer(this);
    timer2->stop();

    TiempoTortuga=new QTimer(this);
    TiempoTortuga->stop();

    TiempoRasengan=new QTimer(this);
    TiempoRasengan->stop();

    TiempoRayo=new QTimer(this);
    TiempoRayo->stop();

    TiempoEnemigoFinal=new QTimer(this);
    TiempoEnemigoFinal->stop();

    TiempoVida=new QTimer(this);
    TiempoVida->stop();

    TiempoMonedas=new QTimer(this);
    TiempoMonedas->stop();

    Choque1= new QTimer(this);
    Choque1->stop();

    Choque2= new QTimer(this);
    Choque2->stop();

    Perdio=new QTimer(this);
    Perdio->stop();
    gano=new QTimer(this);
    gano->stop();


    //connecion de señales y slots
    connect(timer,SIGNAL(timeout()),this,SLOT(actualizar()));
    connect(timer2,SIGNAL(timeout()),this,SLOT(actualizar2()));

    connect(Perdio,SIGNAL(timeout()),this,SLOT(esperar()));
    connect(gano,SIGNAL(timeout()),this,SLOT(ganador()));

    connect(TiempoTortuga,SIGNAL(timeout()),this,SLOT(TortugasRandom()));
    connect(TiempoRasengan,SIGNAL(timeout()),this,SLOT(RasenganRandom()));
    connect(TiempoRayo,SIGNAL(timeout()),this,SLOT(RayosRandom()));
    connect(TiempoEnemigoFinal,SIGNAL(timeout()),this,SLOT(EnemigoFinalRandom()));

    connect(TiempoVida,SIGNAL(timeout()),this,SLOT(VidaRandom()));
    connect(TiempoMonedas,SIGNAL(timeout()),this,SLOT(MonedasRandom()));


    connect(Choque1,SIGNAL(timeout()),this,SLOT(Chok1()));
    connect(Choque2,SIGNAL(timeout()),this,SLOT(Chok2()));


    //Semilla para generar objetos al azar
     srand(time(NULL));

     //Contador elementos coleccionables
     contadorVidas=1;
     distanciaLCD=0;
     MonedasLCD=0;


    //Inicializacion del nivel 1
    numerotortugas=3000;
    numerorasengan=5500;
    numerorayos= 350000000;
    numeroenemigofinal=2000000000;
    numeroVidas= 4000;
    numeromonedas=2000;

}

JuegoPPAL::~JuegoPPAL()
{
    delete timer;
    delete timer2;
    delete scene;
    delete ui;
    delete personaje;
    delete resultadoP;

    tortuga.clear();
    rasengan.clear();
    rayos.clear();
    enemigofinal.clear();
    monedas.clear();
    vida.clear();

    delete TiempoTortuga;
    delete TiempoRasengan;
    delete TiempoRayo;
    delete TiempoEnemigoFinal;

    delete TiempoVida;
    delete TiempoMonedas;

    delete Choque1;
    delete Choque2;
    delete Perdio;
    delete gano;

    delete player;
    delete JuegoPPLMusica;
    delete rayoMusica;
    delete Golpe;
    delete Lose;
    delete clickPPAL;
    delete clickPPAL2;
    delete clickPPAL3;
    delete MonedasMusic;
    delete VidaMusica;


}


//Botones
void JuegoPPAL::on_actionIniciar_triggered()
{
    //musica
    JuegoPPLMusica->play();
    JuegoPPLMusica->setVolume(30);
    rayoMusica->setVolume(100);
    Rasengan->setVolume(30);
    Golpe->setVolume(100);

    ui->Vida->display(contadorVidas);
    ui->Distancia->display(distanciaLCD);
    ui->Monedas->display(MonedasLCD);

    timer->start(1000*dt);
    timer2->start(1000*dt);
    TiempoTortuga->start(numerotortugas);
    TiempoRasengan->start(numerorasengan);
    TiempoRayo->start(numerorayos);
    TiempoEnemigoFinal->start(numeroenemigofinal);

    TiempoVida->start(numeroVidas);
    TiempoMonedas->start(numeromonedas);


}
void JuegoPPAL::on_actionDetener_triggered()
{
    JuegoPPLMusica->stop();

    timer->stop();
    timer2->stop();
    TiempoTortuga->stop();
    TiempoRasengan->stop();
    TiempoRayo->stop();
    TiempoEnemigoFinal->stop();

    TiempoVida->stop();
    TiempoMonedas->stop();


}

void JuegoPPAL::on_actionReincicar_triggered()
{
    if(Multiplayer==true){
        reiniciarMultijugador();
    }
    else {
        reiniciar();
    }

}

void JuegoPPAL::on_actionGuardar_triggered()
{
    clickPPAL->play();

    on_actionDetener_triggered();
    QFile archivo;
    QTextStream escritura;
    QString nombreArchivo;
    nombreArchivo=QFileDialog::getSaveFileName(this,"Guardar Patida","C://Users//Fafoa//Desktop//Juego//Partidas Guardadas");
    archivo.setFileName(nombreArchivo);
    archivo.open(QIODevice::WriteOnly | QIODevice::Text);
    if(!archivo.isOpen()){
        clickPPAL2->play();
        QMessageBox::critical(this,"ERROR","No se pudo guardar la partida!");
    }
    else {
        clickPPAL3->play();
        escritura.setDevice(&archivo);

        escritura<<personaje->getPersonaje()->getPx()<<"\t"
                 <<personaje->getPersonaje()->getPy()<<"\t"
                 <<personaje->getPersonaje()->getVx()<<"\t"
                 <<contadorVidas<<"\t"<<distanciaLCD<<"\t"<<MonedasLCD<<"\t"<<jugador2<<"\t"<<"|"<<"\t";

        for(int i=0;i<tortuga.size();i++){
            escritura<<tortuga.at(i)->getItem()->getPx()<<"\t"
                     <<tortuga.at(i)->getItem()->getPy()<<"\t"
                     <<tortuga.at(i)->getItem()->getVx()<<"\t";

        }
        escritura<<"|"<<"\t";

        for(int i=0;i<rasengan.size();i++){
            escritura<<rasengan.at(i)->getItem()->getPx()<<"\t"
                     <<rasengan.at(i)->getItem()->getPy()<<"\t"
                     <<rasengan.at(i)->getItem()->getVx()<<"\t";
        }
        escritura<<"|"<<"\t";

        for(int i=0;i<rayos.size();i++){
            escritura<<rayos.at(i)->getItem()->getPx()<<"\t"
                     <<rayos.at(i)->getItem()->getPy()<<"\t"
                     <<rayos.at(i)->getItem()->getVx()<<"\t";
        }
        escritura<<"|"<<"\t";

        for(int i=0;i<monedas.size();i++){
            escritura<<monedas.at(i)->getItem()->getPx()<<"\t"
                     <<monedas.at(i)->getItem()->getPy()<<"\t"
                     <<monedas.at(i)->getItem()->getVx()<<"\t";
        }
        escritura<<"|"<<"\t";

        for(int i=0;i<vida.size();i++){
            escritura<<vida.at(i)->getItem()->getPx()<<"\t"
                     <<vida.at(i)->getItem()->getPy()<<"\t"
                     <<vida.at(i)->getItem()->getVx()<<"\t";
        }
        escritura<<"|"<<"\t";

        for(int i=0;i<enemigofinal.size();i++){
            escritura<<enemigofinal.at(i)->getItem()->getPx()<<"\t"
                     <<enemigofinal.at(i)->getItem()->getPy()<<"\t"
                     <<enemigofinal.at(i)->getItem()->getVx()<<"\t";
        }
        escritura<<"|"<<"\t";

        archivo.close();
        QMessageBox::information(this,tr("Guardado"),tr("->Partida guardada con exito!\n"
                                                             "->Para continuar la partida, haz click en iniciar."));
    }

}

void JuegoPPAL::dificultad()
{
    if(personaje->getPersonaje()->getPx()>=7300 && ban == false){
        Nivel2->play();
        TiempoTortuga->start(1500);
        TiempoRasengan->start(3000);
        TiempoRayo->start(7000);

        TiempoVida->start(5500);

        personaje->getPersonaje()->setVx(400);
        ban=true;
    }

    if(personaje->getPersonaje()->getPx()>=16900 && ban1 == false){
        nivel3->play();
        TiempoTortuga->start(1000);
        TiempoRasengan->start(2000);
        TiempoRayo->start(6500);
        TiempoEnemigoFinal->start(8000);
        TiempoVida->start(8000);

        personaje->getPersonaje()->setVx(850);
        ban1=true;
    }

}

void JuegoPPAL::actualizar2()
{
    focus();
    personaje->actualizar(dt);
}

void JuegoPPAL::actualizar()
{
    l=serial.read(&data,1);
    if(l!=0){
        if(data=='1'){
            personaje->getPersonaje()->setPy(personaje->getPersonaje()->getPy()+20);
        }
        if(data=='2'){
            personaje->getPersonaje()->setPy(personaje->getPersonaje()->getPy()-20);
        }

        if(data=='4'){
            JuegoPPLMusica->stop();
            timer2->stop();
            timer->stop();
            TiempoTortuga->stop();
            TiempoRasengan->stop();
            TiempoRayo->stop();
            TiempoEnemigoFinal->stop();

            TiempoVida->stop();
            TiempoMonedas->stop();
        }
        if(data=='5'){
            personaje->getPersonaje()->setPx(personaje->getPersonaje()->getPx()+25);
        }
    }
//************************************************************************************
    for(int i=0;i<tortuga.size();i++){
        tortuga.at(i)->actualizar(dt);
    }

    for(int i=0;i<rasengan.size();i++){
        rasengan.at(i)->actualizar(dt);
    }

    for(int i=0;i<rayos.size();i++){
        rayos.at(i)->actualizar(dt);
    }


    for(int i=0;i<enemigofinal.size();i++){
        enemigofinal.at(i)->actualizar(dt);
        if(enemigofinal.front()->getItem()->getPx()<personaje->getPersonaje()->getPx()-500){
            scene->removeItem(enemigofinal.at(i));
            enemigofinal.removeAt(i);
        }
        if(enemigofinal.front()->getItem()->getPx()==personaje->getPersonaje()->getPx()+100 && enemigofinal.front()->getItem()->getPy()==personaje->getPersonaje()->getPy()){
            scene->removeItem(enemigofinal.at(i));
            enemigofinal.removeAt(i);
        }
    }

    for(int i=0;i<vida.size();i++){
        vida.at(i)->actualizar(dt);
    }

    for(int i=0;i<monedas.size();i++){
        monedas.at(i)->actualizarMoneda(dt);

    }
//************************************************************************************

    if(personaje->getPersonaje()->getPx()>=29000){
        if(jugador2){
            JuegoPPLMusica->setVolume(5);
            rayoMusica->setVolume(30);
            Rasengan->setVolume(10);
            Golpe->setVolume(30);

            timer->stop();
            timer2->stop();
            TiempoTortuga->stop();
            TiempoRasengan->stop();
            TiempoRayo->stop();
            TiempoEnemigoFinal->stop();

            TiempoMonedas->stop();
            TiempoVida->stop();

            resultadoP->op=10;
            resultadoP->imaganes();
            QTimer::singleShot(820,resultadoP,SLOT(showMaximized()));

            QTimer::singleShot(810,this,SLOT(ocultar()));

            Lose->setMedia(QUrl("qrc:/MusicaJuego/You Win.mp3"));
            Lose->setVolume(150);
            Lose->play();

            QTimer::singleShot(8000,this,SLOT(esperar()));
            QTimer::singleShot(8000,this,SLOT(parar()));
            QTimer::singleShot(8500,resultadoP,SLOT(close()));

        }
        else {
            JuegoPPLMusica->setVolume(5);
            rayoMusica->setVolume(30);
            Rasengan->setVolume(10);
            Golpe->setVolume(30);

            timer->stop();
            timer2->stop();
            TiempoTortuga->stop();
            TiempoRasengan->stop();
            TiempoRayo->stop();
            TiempoEnemigoFinal->stop();

            TiempoMonedas->stop();
            TiempoVida->stop();


            resultadoP->op=9;
            resultadoP->imaganes();

            QTimer::singleShot(820,resultadoP,SLOT(showMaximized()));

            QTimer::singleShot(810,this,SLOT(ocultar()));

            Lose->setMedia(QUrl("qrc:/MusicaJuego/You Win.mp3"));
            Lose->setVolume(150);
            Lose->play();

            QTimer::singleShot(8000,this,SLOT(esperar()));
            QTimer::singleShot(8000,this,SLOT(parar()));
            QTimer::singleShot(8500,resultadoP,SLOT(close()));

        }
    }


   //facus personaje y movimiento
    distanciaLCD=personaje->getPersonaje()->getPx();
    ui->Vida->display(contadorVidas);
    ui->Distancia->display(distanciaLCD);
    ui->Monedas->display(MonedasLCD);

    colision(personaje);
    dificultad();

    //Guardar datos en ejecucion
    fstream escritura;
    escritura.open("Guardar.txt",ios::out);
    escritura<<personaje->getPersonaje()->getPx()<<"\t"
             <<personaje->getPersonaje()->getPy()<<"\t"
             <<personaje->getPersonaje()->getVx()<<"\t"
            <<contadorVidas<<"\t"<<distanciaLCD<<"\t"<<MonedasLCD<<"\t";
   escritura.close();

}


void JuegoPPAL::focus()
{
    if(personaje->getPersonaje()->getPx()>=500){
        scene->setSceneRect(personaje->getPersonaje()->getPx()-300,0,1000,496);
        ui->graphicsView->setScene(scene);
    }
    if(personaje->getPersonaje()->getPy()>=380){
        personaje->getPersonaje()->setPy(380);
    }
    if(personaje->getPersonaje()->getPy()<=0){
        personaje->getPersonaje()->setPy(0);
    }
}

void JuegoPPAL::keyPressEvent(QKeyEvent *event)
{
    if(event->key()==Qt::Key_W){
        personaje->getPersonaje()->setPy(personaje->getPersonaje()->getPy()+15);
    }
    if(event->key()==Qt::Key_S){
        personaje->getPersonaje()->setPy(personaje->getPersonaje()->getPy()-15);
    }
    if(event->key()==Qt::Key_D){
        personaje->getPersonaje()->setPx(personaje->getPersonaje()->getPx()+30);
    }

    if(event->key()==Qt::Key_P){
        if(timer->isActive()){
            timer->stop();
            TiempoTortuga->stop();
            TiempoRayo->stop();
            TiempoVida->stop();
            TiempoMonedas->stop();
        }
        else{
            //timer->start(1000*dt);
            ui->Vida->display(contadorVidas);
            timer->start(1000*dt);
            TiempoTortuga->start(numerotortugas);
            TiempoRayo->start(numerorayos);

            TiempoVida->start(numeroVidas);
            TiempoMonedas->start(numeromonedas);
        }
    }
}


void JuegoPPAL::colision(personajegraf *a)   //falta terminar las colisiones
{

    for(int i=0;i<tortuga.size();i++){
        if(personaje->collidesWithItem(tortuga.at(i))){
            Golpe->play();
            calcularPuntaje();
            if(dosjugadores){
                resultadoP->multi=true;
                if(jugador2){
                    if(contadorVidas>0){
                        personaje->getPersonaje()->setVx(personaje->getPersonaje()->getVx()*-1);
                        QTimer::singleShot(800,this,SLOT(Retro()));

                        scene->removeItem(tortuga.at(i));
                        tortuga.removeAt(i);
                        contadorVidas--;
                        personaje->choque2();
                        Choque2->start(800);
                    }
                    //muere el personaje 2
                    else {
                        timer->stop();
                        TiempoTortuga->stop();
                        TiempoRasengan->stop();
                        TiempoMonedas->stop();
                        TiempoRayo->stop();
                        TiempoEnemigoFinal->stop();

                        TiempoVida->stop();
                        scene->removeItem(tortuga.at(i));
                        explosion->play();

                        personaje->muerte();
                        player->stop();
                        QTimer::singleShot(820,this,SLOT(ocultar()));

                        //Muetsra en pantalla los resultados
                        resultadoP->op=4;
                        resultadoP->imaganes();
                        QTimer::singleShot(840,resultadoP,SLOT(showMaximized()));
                        QTimer::singleShot(4500,this,SLOT(ganador()));

                        jugador2=false;
                    }
                }
                //JUGADOR 1
                else {
                    if(contadorVidas>0){
                        personaje->getPersonaje()->setVx(personaje->getPersonaje()->getVx()*-1);
                        QTimer::singleShot(800,this,SLOT(Retro()));

                        scene->removeItem(tortuga.at(i));
                        tortuga.removeAt(i);
                        contadorVidas--;
                        personaje->choque1();
                        Choque1->start(800);
                    }
                    //MUERE PERSONAJE 1
                    else {
                        timer->stop();
                        TiempoTortuga->stop();
                        TiempoRasengan->stop();
                        TiempoMonedas->stop();
                        TiempoRayo->stop();
                        TiempoEnemigoFinal->stop();

                        TiempoVida->stop();
                        scene->removeItem(tortuga.at(i));
                        explosion->play();

                        personaje->muerte();
                        QTimer::singleShot(820,this,SLOT(ocultar()));

                        //Muetsra en pantalla los resultados
                        resultadoP->op=1;
                        resultadoP->imaganes();
                        QTimer::singleShot(840,resultadoP,SLOT(showMaximized()));

                        QTimer::singleShot(7000,this,SLOT(esperar()));
                        QTimer::singleShot(7000,this,SLOT(showMaximized()));
                        QTimer::singleShot(7500,resultadoP,SLOT(close()));
                    }
                }
            }
            //CUANDO ES SOLO UN JUGADOR
            else {
                if(jugador2){
                    if(contadorVidas>0){
                        personaje->getPersonaje()->setVx(personaje->getPersonaje()->getVx()*-1);
                        QTimer::singleShot(800,this,SLOT(Retro()));

                        scene->removeItem(tortuga.at(i));
                        tortuga.removeAt(i);
                        contadorVidas--;
                        personaje->choque2();
                        Choque2->start(800);
                    }
                    else {
                        //CUANDO MUERE EL PERSONAJE 2                     
                        timer->stop();                      
                        TiempoTortuga->stop();
                        TiempoRasengan->stop();
                        TiempoMonedas->stop();
                        TiempoRayo->stop();
                        TiempoEnemigoFinal->stop();

                        TiempoVida->stop();
                        scene->removeItem(tortuga.at(i));

                        explosion->play();
                        JuegoPPLMusica->setVolume(10);

                        personaje->muerte();
                        QTimer::singleShot(820,this,SLOT(ocultar()));

                        //Muetsra en pantalla los resultados
                        resultadoP->op=4;
                        resultadoP->imaganes();
                        QTimer::singleShot(840,resultadoP,SLOT(showMaximized()));

                        QTimer::singleShot(7000,this,SLOT(esperar()));
                        QTimer::singleShot(7000,this,SLOT(parar()));
                        QTimer::singleShot(7500,resultadoP,SLOT(close()));
                    }
                }
                else {  //JUGADOR 1
                    if(contadorVidas>0){
                        personaje->getPersonaje()->setVx(personaje->getPersonaje()->getVx()*-1);
                        QTimer::singleShot(800,this,SLOT(Retro()));

                        scene->removeItem(tortuga.at(i));
                        tortuga.removeAt(i);
                        contadorVidas--;
                        personaje->choque1();
                        Choque1->start(800);
                    }
                    else {
                        //CUANDO MUERE EL  JUGADOR 1
                        timer->stop();
                        TiempoTortuga->stop();
                        TiempoRasengan->stop();
                        TiempoMonedas->stop();
                        TiempoRayo->stop();
                        TiempoEnemigoFinal->stop();

                        TiempoVida->stop();
                        scene->removeItem(tortuga.at(i));

                        explosion->play();
                        JuegoPPLMusica->setVolume(10);

                        personaje->muerte();
                        QTimer::singleShot(1100,this,SLOT(ocultar()));

                        resultadoP->op=1;
                        resultadoP->imaganes();
                        QTimer::singleShot(1120,resultadoP,SLOT(showMaximized()));

                        QTimer::singleShot(7000,this,SLOT(esperar()));
                        QTimer::singleShot(7000,this,SLOT(parar()));
                        QTimer::singleShot(7500,resultadoP,SLOT(close()));
                    }
                }
            }
        }
    }

//***********************************************************************************
    //Colisión Rasengan

    for(int i=0;i<rasengan.size();i++){
        if(personaje->collidesWithItem(rasengan.at(i))){
            personaje->getPersonaje()->setPx(personaje->getPersonaje()->getPx()-100);
            Rasengan->play();
            calcularPuntaje();
            if(dosjugadores){
                resultadoP->multi=true;
                if(jugador2){
                    if(contadorVidas>0){
                        personaje->getPersonaje()->setVx(personaje->getPersonaje()->getVx()*-1);
                        QTimer::singleShot(800,this,SLOT(Retro()));

                        scene->removeItem(rasengan.at(i));
                        rasengan.removeAt(i);
                        contadorVidas--;
                        personaje->choque2();
                        Choque2->start(800);
                    }
                    //Muere el personaje 2
                    else {
                        timer->stop();
                        TiempoTortuga->stop();
                        TiempoRasengan->stop();
                        TiempoMonedas->stop();
                        TiempoRayo->stop();
                        TiempoEnemigoFinal->stop();

                        TiempoVida->stop();
                        scene->removeItem(rasengan.at(i));
                        explosion->play();

                        personaje->muerte2();
                        player->stop();
                        QTimer::singleShot(820,this,SLOT(ocultar()));

                       //Muestra en pantalla los resultados
                       resultadoP->op=5;
                       resultadoP->imaganes();
                       QTimer::singleShot(840,resultadoP,SLOT(showMaximized()));
                       QTimer::singleShot(4500,this,SLOT(ganador()));

                       jugador2=false;
                    }
                }
                else {
                    if(contadorVidas>0){
                        personaje->getPersonaje()->setVx(personaje->getPersonaje()->getVx()*-1);
                        QTimer::singleShot(800,this,SLOT(Retro()));

                        scene->removeItem(rasengan.at(i));
                        rasengan.removeAt(i);
                        contadorVidas--;
                        personaje->choque1();
                        Choque1->start(800);
                    }
                    //Muere el personaje 1
                    else {
                        timer->stop();
                        TiempoTortuga->stop();
                        TiempoRasengan->stop();
                        TiempoRayo->stop();
                        TiempoEnemigoFinal->stop();

                        TiempoMonedas->stop();
                        TiempoVida->stop();
                        scene->removeItem(rasengan.at(i));                      
                        explosion->play();

                        personaje->muerte2(); //muerte2

                        //Muestra en pantalla los resultados
                        resultadoP->op=2;
                        resultadoP->imaganes();
                        QTimer::singleShot(840,resultadoP,SLOT(showMaximized()));

                        QTimer::singleShot(7000,this,SLOT(esperar()));
                        QTimer::singleShot(7000,this,SLOT(showMaximized()));
                        QTimer::singleShot(7500,resultadoP,SLOT(close()));
                    }
                }
            }
            //Cuando solo es un jugador!
            else {
                if(jugador2){
                    if(contadorVidas>0){
                        personaje->getPersonaje()->setVx(personaje->getPersonaje()->getVx()*-1);
                        QTimer::singleShot(800,this,SLOT(Retro()));

                        scene->removeItem(rasengan.at(i));
                        rasengan.removeAt(i);
                        contadorVidas--;
                        personaje->choque2();
                        Choque2->start(800);
                    }
                    //Cuando muere el personaje 2
                    else {
                        timer->stop();
                        TiempoTortuga->stop();
                        TiempoRasengan->stop();
                        TiempoRayo->stop();
                        TiempoEnemigoFinal->stop();

                        TiempoMonedas->stop();
                        TiempoVida->stop();
                        scene->removeItem(rasengan.at(i));


                        explosion->play();
                        JuegoPPLMusica->setVolume(10);

                        personaje->muerte2(); //muerte2
                        QTimer::singleShot(820,this,SLOT(ocultar()));

                        resultadoP->op=5;
                        resultadoP->imaganes();
                        QTimer::singleShot(840,resultadoP,SLOT(showMaximized()));

                        QTimer::singleShot(7000,this,SLOT(esperar()));
                        QTimer::singleShot(7000,this,SLOT(parar()));
                        QTimer::singleShot(7500,resultadoP,SLOT(close()));
                    }
                }
                //Personaje 1
                else {
                    if(contadorVidas>0){
                        personaje->getPersonaje()->setVx(personaje->getPersonaje()->getVx()*-1);
                        QTimer::singleShot(800,this,SLOT(Retro()));

                        scene->removeItem(rasengan.at(i));
                        rasengan.removeAt(i);
                        contadorVidas--;
                        personaje->choque1();
                        Choque1->start(800);
                    }
                    //CUando muere el personaje 1
                    else {
                        timer->stop();
                        TiempoTortuga->stop();
                        TiempoRasengan->stop();
                        TiempoRayo->stop();
                        TiempoEnemigoFinal->stop();

                        TiempoMonedas->stop();
                        TiempoVida->stop();
                        scene->removeItem(rasengan.at(i));

                        explosion->play();
                        JuegoPPLMusica->setVolume(10);

                        personaje->muerte2(); //muerte2
                        QTimer::singleShot(1100,this,SLOT(ocultar()));

                        resultadoP->op=2;
                        resultadoP->imaganes();
                        QTimer::singleShot(1120,resultadoP,SLOT(showMaximized()));

                        QTimer::singleShot(7000,this,SLOT(esperar()));
                        QTimer::singleShot(7000,this,SLOT(parar()));
                        QTimer::singleShot(7500,resultadoP,SLOT(close()));

                    }
                }
            }
        }
    }

//***********************************************************************************
    //Colision con Rayos.

    for(int i=0;i<rayos.size();i++){
        if(personaje->collidesWithItem(rayos.at(i))){
            personaje->getPersonaje()->setPx(personaje->getPersonaje()->getPx()-100);
            rayoMusica->play();
            calcularPuntaje();
            if(dosjugadores){
                resultadoP->multi=true;
                if(jugador2){
                    if(contadorVidas>0){
                        personaje->getPersonaje()->setVx(personaje->getPersonaje()->getVx()*-1);
                        QTimer::singleShot(800,this,SLOT(Retro()));

                        scene->removeItem(rayos.at(i));
                        rayos.removeAt(i);
                        contadorVidas=0;
                        personaje->choque2();
                        Choque2->start(800);
                    }
                    //Muere personaje 2
                    else {
                        timer->stop();
                        TiempoTortuga->stop();
                        TiempoRasengan->stop();
                        TiempoRayo->stop();
                        TiempoEnemigoFinal->stop();

                        TiempoMonedas->stop();
                        TiempoVida->stop();
                        scene->removeItem(rayos.at(i));
                        explosion->play();

                        personaje->muerte3(); //muerte3
                        player->stop();
                        QTimer::singleShot(820,this,SLOT(ocultar()));

                        //Muestra pantalla de resultados
                        resultadoP->op=6;
                        resultadoP->imaganes();
                        QTimer::singleShot(840,resultadoP,SLOT(showMaximized()));
                        QTimer::singleShot(4500,this,SLOT(ganador()));
                        jugador2=false;
                    }
                }
                //Personaje 1
                else {
                    if(contadorVidas>0){
                        personaje->getPersonaje()->setVx(personaje->getPersonaje()->getVx()*-1);
                        QTimer::singleShot(800,this,SLOT(Retro()));

                        scene->removeItem(rayos.at(i));
                        rayos.removeAt(i);
                        contadorVidas=0;
                        personaje->choque1();
                        Choque1->start(800);
                    }
                    //Muere el personaje 1
                    else {
                        timer->stop();
                        TiempoTortuga->stop();
                        TiempoRasengan->stop();
                        TiempoMonedas->stop();
                        TiempoRayo->stop();
                        TiempoEnemigoFinal->stop();

                        TiempoVida->stop();
                        scene->removeItem(rayos.at(i));
                        explosion->play();

                        personaje->muerte3(); //muerte3

                        QTimer::singleShot(820,this,SLOT(ocultar()));

                        //muestra pantalla de resultados
                        resultadoP->op=3;
                        resultadoP->imaganes();
                        QTimer::singleShot(840,resultadoP,SLOT(showMaximized()));

                        QTimer::singleShot(7000,this,SLOT(esperar()));
                        QTimer::singleShot(7000,this,SLOT(showMaximized()));
                        QTimer::singleShot(7500,resultadoP,SLOT(close()));
                    }
                }
            }
            //Cuando solo es un jugador
            else {
                if(jugador2){
                    if(contadorVidas>0){
                        personaje->getPersonaje()->setVx(personaje->getPersonaje()->getVx()*-1);
                        QTimer::singleShot(800,this,SLOT(Retro()));

                        scene->removeItem(rayos.at(i));
                        rayos.removeAt(i);
                        contadorVidas=0;
                        personaje->choque2();
                        Choque2->start(800);
                    }
                    else {
                        //cuando muere el personaje 2
                        timer->stop();
                        TiempoTortuga->stop();
                        TiempoRasengan->stop();
                        TiempoMonedas->stop();
                        TiempoRayo->stop();
                        TiempoEnemigoFinal->stop();

                        TiempoVida->stop();
                        scene->removeItem(rayos.at(i));

                        explosion->play();
                        JuegoPPLMusica->setVolume(10);

                        personaje->muerte3(); //muerte3
                        QTimer::singleShot(820,this,SLOT(ocultar()));

                        resultadoP->op=6;
                        resultadoP->imaganes();
                        QTimer::singleShot(840,resultadoP,SLOT(showMaximized()));

                        QTimer::singleShot(7000,this,SLOT(esperar()));
                        QTimer::singleShot(7000,this,SLOT(parar()));
                        QTimer::singleShot(7500,resultadoP,SLOT(close()));

                    }
                }
                else {
                    if(contadorVidas>0){
                        personaje->getPersonaje()->setVx(personaje->getPersonaje()->getVx()*-1);
                        QTimer::singleShot(800,this,SLOT(Retro()));

                        scene->removeItem(rayos.at(i));
                        rayos.removeAt(i);
                        contadorVidas=0;
                        personaje->choque1();
                        Choque1->start(800);
                    }
                    else {
                        //cuando muere el personaje 1
                        timer->stop();
                        TiempoTortuga->stop();
                        TiempoRasengan->stop();
                        TiempoMonedas->stop();
                        TiempoRayo->stop();
                        TiempoEnemigoFinal->stop();

                        TiempoVida->stop();
                        scene->removeItem(rayos.at(i));

                        explosion->play();
                        JuegoPPLMusica->setVolume(10);

                        personaje->muerte3(); //muerte3
                        QTimer::singleShot(1100,this,SLOT(ocultar()));

                        resultadoP->op=3;
                        resultadoP->imaganes();
                        QTimer::singleShot(1120,resultadoP,SLOT(showMaximized()));

                        QTimer::singleShot(7000,this,SLOT(esperar()));
                        QTimer::singleShot(7000,this,SLOT(parar()));
                        QTimer::singleShot(7500,resultadoP,SLOT(close()));
                    }
                }
            }
        }
    }
//*****************************************************************************************
    //Colision con Enemigo final.

    for(int i=0;i<enemigofinal.size();i++){
        if(personaje->collidesWithItem(enemigofinal.at(i))){
            calcularPuntaje();
            if(dosjugadores){
                resultadoP->multi=true;
                if(jugador2){
                    //Muere personaje 2
                    timer->stop();
                    TiempoTortuga->stop();
                    TiempoRasengan->stop();
                    TiempoEnemigoFinal->stop();
                    TiempoMonedas->stop();
                    TiempoRayo->stop();
                    TiempoVida->stop();
                    scene->removeItem(enemigofinal.at(i));
                    explosion->play();

                    personaje->muerte4();
                    player->stop();
                    QTimer::singleShot(820,this,SLOT(ocultar()));

                    resultadoP->op=12;
                    resultadoP->imaganes();
                    QTimer::singleShot(840,resultadoP,SLOT(showMaximized()));
                    QTimer::singleShot(4500,this,SLOT(ganador()));

                    jugador2=false;
                }
                //Jugador1
                else {
                    //muere Jugador 1
                    timer->stop();
                    TiempoTortuga->stop();
                    TiempoRasengan->stop();
                    TiempoEnemigoFinal->stop();
                    TiempoMonedas->stop();
                    TiempoRayo->stop();
                    TiempoVida->stop();
                    scene->removeItem(enemigofinal.at(i));

                    explosion->play();

                    personaje->muerte4();
                    QTimer::singleShot(820,this,SLOT(ocultar()));

                    //Muestra en pantalla los resultados
                    resultadoP->op=11;
                    resultadoP->imaganes();
                    QTimer::singleShot(840,resultadoP,SLOT(showMaximized()));

                    QTimer::singleShot(7000,this,SLOT(esperar()));
                    QTimer::singleShot(7000,this,SLOT(showMaximized()));
                    QTimer::singleShot(7500,resultadoP,SLOT(close()));
                }
            }
            //Cuando es solo un jugador
            else {
                if(jugador2){
                    timer->stop();
                    TiempoTortuga->stop();
                    TiempoRasengan->stop();
                    TiempoEnemigoFinal->stop();
                    TiempoMonedas->stop();
                    TiempoRayo->stop();
                    TiempoVida->stop();
                    scene->removeItem(enemigofinal.at(i));

                    explosion->play();
                    JuegoPPLMusica->setVolume(10);

                    personaje->muerte4();
                    QTimer::singleShot(820,this,SLOT(ocultar()));

                    resultadoP->op=12;
                    resultadoP->imaganes();
                    QTimer::singleShot(840,resultadoP,SLOT(showMaximized()));

                    QTimer::singleShot(7000,this,SLOT(esperar()));
                    QTimer::singleShot(7000,this,SLOT(parar()));
                    QTimer::singleShot(7500,resultadoP,SLOT(close()));
                }
                else {
                    timer->stop();
                    TiempoTortuga->stop();
                    TiempoRasengan->stop();
                    TiempoEnemigoFinal->stop();
                    TiempoMonedas->stop();
                    TiempoRayo->stop();
                    TiempoVida->stop();
                    scene->removeItem(enemigofinal.at(i));
                   //********************************

                    explosion->play();
                    JuegoPPLMusica->setVolume(10);

                    personaje->muerte4();

                    QTimer::singleShot(1100,this,SLOT(ocultar()));
                    resultadoP->op=11;
                    resultadoP->imaganes();
                    QTimer::singleShot(1120,resultadoP,SLOT(showMaximized()));

                    QTimer::singleShot(7000,this,SLOT(esperar()));
                    QTimer::singleShot(7000,this,SLOT(parar()));
                    QTimer::singleShot(7500,resultadoP,SLOT(close()));

                }
            }
        }
    }

//***********************************************************************************

    //Colision vida
    for(int i=0;i<vida.size();i++){
        if(a->collidesWithItem(vida.at(i))){
            VidaMusica->play();
            if(contadorVidas==3){
               scene->removeItem(vida.at(i));
               vida.removeAt(i);

            }
            else if (contadorVidas<3) {
                scene->removeItem(vida.at(i));
                vida.removeAt(i);
                contadorVidas++;
            }
        }
    }
//***********************************************************************************
        //colision con moneda
        for(int i=0;i<monedas.size();i++){
            if(a->collidesWithItem(monedas.at(i))){
                MonedasMusic->play();
                scene->removeItem(monedas.at(i));
                monedas.removeAt(i);
                MonedasLCD+=20;
            }
        }
}



//Quitando elementos
void JuegoPPAL::quitarelementos()
{
    for(int i=0; i<tortuga.length();i++){
        scene->removeItem(tortuga.at(i));
    }

    for(int i=0;i<rasengan.length();i++){
        scene->removeItem(rasengan.at(i));
    }

    for(int i=0;i<rayos.length();i++){
        scene->removeItem(rayos.at(i));
    }

    for(int i=0;i<enemigofinal.length();i++){
        scene->removeItem(enemigofinal.at(i));
    }

    for(int i=0;i<vida.length();i++){
        scene->removeItem(vida.at(i));
    }

    for(int i=0;i<monedas.length();i++){
        scene->removeItem(monedas.at(i));
    }

    scene->removeItem(personaje);
}

void JuegoPPAL::borrarelementos()
{
    tortuga.clear();
    rasengan.clear();
    rayos.clear();
    enemigofinal.clear();

    vida.clear();
    monedas.clear();


    delete personaje;
}

//Objetos random**************************************************************************
void JuegoPPAL::TortugasRandom()
{
    float py=0,vx=0;
    py=rand() % 350+50;//posicion en y
    vx=rand() % 150+80;

    tortuga.append(new obstaculoGraf(personaje->getPersonaje()->getPx()+1000,py));
    tortuga.last()->moverTortuga();
    tortuga.last()->getItem()->setVel(vx,0);
    scene->addItem(tortuga.last());

    if(tortuga.front()->getItem()->getPx()<=0){
        scene->removeItem(tortuga.front());
        tortuga.pop_front();
    }
}

void JuegoPPAL::RasenganRandom()
{
    float py=0,vx=0;
    py=rand() % 350+100;
    vx=rand() % 150+80;
    rasengan.append(new obstaculoGraf(personaje->getPersonaje()->getPx()+800,py));
    rasengan.last()->moverRasengan();
    rasengan.last()->getItem()->setVel(vx,0);
    scene->addItem(rasengan.last());

    if(rasengan.front()->getItem()->getPx()+100<personaje->getPersonaje()->getPx()){
        scene->removeItem(rasengan.front());
        rasengan.pop_front();
    }
}

void JuegoPPAL::RayosRandom()
{
    float py=0;
    py= rand() % 300+50;
    rayos.append(new obstaculoGraf(personaje->getPersonaje()->getPx()+800,py));
    rayos.last()->moverRayos();
    scene->addItem(rayos.last());

    if(rayos.front()->getItem()->getPx()<=300){
        scene->removeItem(rayos.front());
        rayos.pop_front();
    }
}

void JuegoPPAL::EnemigoFinalRandom()
{
    float py=0,vx=0;
    py= rand() % 300+10;
    vx= rand() % 200+150;
    enemigofinal.append(new obstaculoGraf(personaje->getPersonaje()->getPx()+1000,py));
    enemigofinal.last()->moverEnemigoFinal();
    enemigofinal.last()->getItem()->setVel(vx,0);
    scene->addItem(enemigofinal.last());

    if(enemigofinal.front()->getItem()->getPx()<=0){
        scene->removeItem(enemigofinal.front());
        enemigofinal.pop_front();
    }

}

void JuegoPPAL::VidaRandom()
{
    float py=0;
    py=rand() % 300+0;
    vida.append(new obstaculoGraf(personaje->getPersonaje()->getPx()+1000,py));
    vida.last()->moverVida();
    vida.last()->getItem()->setVel(0,0);
    scene->addItem(vida.last());

    if(vida.front()->getItem()->getPx() <=500){
        scene->removeItem(vida.front());
        vida.pop_front();
    }
}

void JuegoPPAL::MonedasRandom()
{
    float py=0,vx=0,vy=0;
    py=rand() % 450+250;
    vx=350;
    vy=600;

    monedas.append(new obstaculoGraf(personaje->getPersonaje()->getPx()+1000,py));

    monedas.last()->moverMoneda();
    monedas.last()->getItem()->setVel(vx,vy);
    scene->addItem(monedas.last());

    if(monedas.front()->getItem()->getPx() <= 0){
        scene->removeItem(monedas.front());
        monedas.pop_front();
    }
}



//Opciones multijugador****************************************************************
void JuegoPPAL::unjugador()
{
    if(cargar==true){
        ponerValores();
        if(jugador2){
            personaje=new personajegraf(posX,posY);
            personaje->volar2();
            scene->addItem(personaje);
            personaje->getPersonaje()->setVx(velocidadNivel1);
        }
        else{
            personaje=new personajegraf(posX,posY);
            personaje->volar1();
            scene->addItem(personaje);
            personaje->getPersonaje()->setVx(velocidadNivel1);
        }
    }
    else{
        if(jugador2){
            personaje=new personajegraf(0,0);
            personaje->volar2();
            scene->addItem(personaje);
            personaje->getPersonaje()->setVx(velocidadNivel1);
        }
        else{
            personaje=new personajegraf(0,0);
            personaje->volar1();
            scene->addItem(personaje);
            personaje->getPersonaje()->setVx(velocidadNivel1);
        }
    }
}

void JuegoPPAL::multijugador()
{
    if(dosjugadores){
        if(jugador2){
            personaje=new personajegraf(0,0);
            personaje->volar2();
            scene->addItem(personaje);
            personaje->getPersonaje()->setVx(velocidadNivel1);

        }
        else {
            personaje=new personajegraf(0,0);
            personaje->volar1();
            scene->addItem(personaje);
            personaje->getPersonaje()->setVx(velocidadNivel1);

        }
    }
    else {
        unjugador();
    }
}



void JuegoPPAL::setDosjugadores(bool value)
{
    dosjugadores=value;
    multijugador();
}

void JuegoPPAL::parar()
{
    reiniciar();
    Menu* menu;
    menu = new Menu();
    menu->showMaximized();

    this->close();
}

void JuegoPPAL::esperar()
{
    if(dosjugadores){
        if(jugador2){
            gano->start(2000);
            jugador2=false;
            reiniciar();
        }
        else {
            jugador2=true;
            reiniciar();
        }
    }
    else {
        reiniciar();
    }
}

void JuegoPPAL::ganador()
{
    if(puntaje1<puntaje2){
        resultadoP->op=8;
        resultadoP->imaganes();
        resultadoP->show();
        win2->play();
        JuegoPPAL::hide();
        QTimer::singleShot(14000,this,SLOT(parar()));
        QTimer::singleShot(15000,resultadoP,SLOT(close()));

        reiniciar();
    }
    else {
        resultadoP->op=7;
        resultadoP->imaganes();
        resultadoP->show();
        win2->play();
        JuegoPPAL::hide();
        QTimer::singleShot(14000,this,SLOT(parar()));
        QTimer::singleShot(15000,resultadoP,SLOT(close()));

        reiniciar();
    }
}

void JuegoPPAL::ocultar()  // falta
{
    JuegoPPAL::hide();

    if(Multiplayer==false){
        Lose->play();
    }
}

void JuegoPPAL::calcularPuntaje()
{
    if(jugador2){
        puntaje2=distanciaLCD*2+(MonedasLCD*8);
    }
    else {
        puntaje1=distanciaLCD*2+(MonedasLCD*8);
    }
}

void JuegoPPAL::reiniciarMultijugador()
{
    jugador2=false;
    timer->stop();
    timer2->stop();
    Perdio->stop();
    JuegoPPLMusica->stop();

    TiempoTortuga->stop();
    TiempoRasengan->stop();
    TiempoRayo->stop();
    TiempoEnemigoFinal->stop();

    TiempoVida->stop();
    TiempoMonedas->stop();

    contadorVidas=1;
    distanciaLCD=0;
    MonedasLCD=0;
    gano->stop();
    quitarelementos();
    borrarelementos();
    scene->setSceneRect(24,0,1000,496);
    multijugador();
}

//*********************************************************************************************
void JuegoPPAL::Chok1()
{
   // personaje->getPersonaje()->setVx(personaje->getPersonaje()->getVx()*-1);
    personaje->volar1();
    Choque1->stop();

}
void JuegoPPAL::Chok2()
{
   // personaje->getPersonaje()->setVx(personaje->getPersonaje()->getVx()*-1);
    personaje->volar2();
    Choque2->stop();
}

void JuegoPPAL::Retro()
{
    personaje->getPersonaje()->setVx(personaje->getPersonaje()->getVx()*-1);



}

void JuegoPPAL::reiniciar()
{
    cargar=false;
    timer->stop();
    timer2->stop();
    Perdio->stop();
    JuegoPPLMusica->stop();
    gano->stop();

    TiempoTortuga->stop();
    TiempoRasengan->stop();
    TiempoRayo->stop();
    TiempoEnemigoFinal->stop();

    TiempoVida->stop();
    TiempoMonedas->stop();

    contadorVidas=1;
    distanciaLCD=0;
    MonedasLCD=0;


    quitarelementos();
    borrarelementos();
    scene->setSceneRect(24,0,1000,496);
    multijugador();
    ui->Distancia->display(0);
    ui->Vida->display(1);
    ui->Monedas->display(0);
}

void JuegoPPAL::on_Vida_overflow()
{
    ui->Vida->display(contadorVidas);
}

void JuegoPPAL::on_Distancia_overflow()
{
    ui->Distancia->display(distanciaLCD);
}

void JuegoPPAL::on_Monedas_overflow()
{
    ui->Monedas->display(MonedasLCD);
}

void JuegoPPAL::on_actionRegresar_al_menu_triggered()
{
    clickPPAL->play();

    reiniciar();
    Menu* menu;
    menu = new Menu();
    menu->showMaximized();

    this->close();
}

void JuegoPPAL::ponerValores()
{
   // iniciar->posX=cargar.at(0).toFloat();
    if(posX>=500){
        scene->setSceneRect(posX-300,0,1000,496);
    }


    for(int i=0;i<tort.size();i+=3){
        tortuga.append(new obstaculoGraf(tort.at(i),tort.at(i+1)));
        tortuga.last()->moverTortuga();
        tortuga.last()->getItem()->setVel(150,0);
        scene->addItem(tortuga.last());
    }

    for(int i=0;i<raseng.size();i+=3){
        rasengan.append(new obstaculoGraf(raseng.at(i),raseng.at(i+1)));
        rasengan.last()->moverRasengan();
        rasengan.last()->getItem()->setVel(120,0);
        scene->addItem(rasengan.last());
    }

    for(int i=0;i<ray.size();i+=3){
        rayos.append(new obstaculoGraf(ray.at(i),ray.at(i+1)));
        rayos.last()->moverRayos();
        rayos.last()->getItem()->setVel(150,0);
        scene->addItem(rayos.last());
    }
    for(int i=0;i<mon.size();i+=3){
        monedas.append(new obstaculoGraf(mon.at(i),mon.at(i+1)));
        monedas.last()->moverMoneda();
        monedas.last()->getItem()->setVel(350,600);
        scene->addItem(monedas.last());
    }
    for(int i=0;i<vid.size();i+=3){
        vida.append(new obstaculoGraf(vid.at(i),vid.at(i+1)));
        vida.last()->moverVida();
        vida.last()->getItem()->setVel(0,0);
        scene->addItem(vida.last());

    }
    for(int i=0;i<enemi.size();i+=3){
        enemigofinal.append(new obstaculoGraf(enemi.at(i),enemi.at(i+1)));
        enemigofinal.last()->moverEnemigoFinal();
        enemigofinal.last()->getItem()->setVel(150,0);
        scene->addItem(enemigofinal.last());
    }
}



void JuegoPPAL::on_actionBluetooth_triggered()
{
    serial.setPortName("COM4"); //Poner el nombre del puerto, probablemente no sea COM3
    if(serial.open(QIODevice::ReadWrite)){
        //Ahora el puerto seria está abierto
        if(!serial.setBaudRate(QSerialPort::Baud9600)) //Configurar la tasa de baudios
            qDebug()<<serial.errorString();

        if(!serial.setDataBits(QSerialPort::Data8))
            qDebug()<<serial.errorString();

        if(!serial.setParity(QSerialPort::NoParity))
            qDebug()<<serial.errorString();

        if(!serial.setStopBits(QSerialPort::OneStop))
            qDebug()<<serial.errorString();

        if(!serial.setFlowControl(QSerialPort::NoFlowControl))
            qDebug()<<serial.errorString();
        QMessageBox::information(this,"Bluetooth","Bletooth funcionando correctamente!");
    }
    else {
        //clickPPAL2->play();
        clickPPAL2->play();
        QMessageBox::warning(this,"ERROR","El bluetooth no se pudo conectar!");
    }
}
