#include "seleccion.h"
#include "ui_seleccion.h"

Seleccion::Seleccion(QWidget *parent) :QWidget(parent),ui(new Ui::Seleccion)
{
    ui->setupUi(this);
    iniciar= new JuegoPPAL();
    this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);//quitar los bordes y botones
    this->setAttribute(Qt::WA_TranslucentBackground);//ponerla transparente

    player=new QMediaPlayer(this);

    MusicSelec=new QMediaPlayer(this);
    MusicSelec->setMedia(QUrl("qrc:/MusicaJuego/selec.mp3"));
    MusicSelec->setVolume(35);


    click=new QMediaPlayer(this);
    click->setMedia(QUrl("qrc:/MusicaJuego/click.mp3"));
    click->setVolume(100);

    clickPPAL2=new QMediaPlayer(this);
    clickPPAL2->setMedia(QUrl("qrc:/MusicaJuego/error.mp3"));
    clickPPAL2->setVolume(100);


}

Seleccion::~Seleccion()
{
    delete ui;
    delete iniciar;
    delete MusicSelec;
    delete player;
    delete click;
    delete clickPPAL2;
}

void Seleccion::on_Iniciar_clicked()
{

    if(multijuador==false){
        if(p1==0 && p2==0){
            clickPPAL2->play();
            QMessageBox::warning(this,"Cuidado","No has seleccionado ningun personaje!");
        }
        else {
            click->play();
            iniciar->showMaximized();
            iniciar->setDosjugadores(false);
            this->close();
            MusicSelec->pause();
        }
    }
    else {
        iniciar->showMaximized();
        iniciar->setDosjugadores(true);
    }
}

void Seleccion::on_Player1_toggled(bool checked)
{
    if(checked){
        player->setMedia(QUrl("qrc:/MusicaJuego/P1.mp3"));
        player->setVolume(80);
        player->play();

        ui->Player2->close();
        iniciar->jugador2=false;
        p1=1;
    }
    else {
        ui->Player2->show();
        p1=0;
    }
}

void Seleccion::on_Player2_toggled(bool checked)
{
    if(checked){
        player->setMedia(QUrl("qrc:/MusicaJuego/P2.mp3"));
        player->setVolume(80);
        player->play();

        ui->Player1->close();
        iniciar->jugador2=true;
        p2=1;
    }
    else {
        ui->Player1->show();
        p2=0;
    }
}
void Seleccion::on_pushButton_clicked()
{
    click->play();

    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this,"Salir","¿Seguro que quieres salir del selector de personajes?",
                                  QMessageBox::Yes|QMessageBox::No);
    if(reply==QMessageBox::Yes){
        this->close();
    }
}

