#ifndef CODIGOQR_H
#define CODIGOQR_H

#include <QDialog>

namespace Ui {
class CodigoQR;
}

class CodigoQR : public QDialog
{
    Q_OBJECT

public:
    explicit CodigoQR(QWidget *parent = nullptr);
    ~CodigoQR();

private slots:
    void on_pushButton_clicked();

private:
    Ui::CodigoQR *ui;
};

#endif // CODIGOQR_H
