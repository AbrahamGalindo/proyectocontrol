#include "inicio.h"
#include "ui_inicio.h"

inicio::inicio(QWidget *parent) :QWidget(parent),ui(new Ui::inicio)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);//quitar los bordes y botones
    this->setAttribute(Qt::WA_TranslucentBackground);//ponerla transparente

    QPixmap imagen(":/ImagenesJuego/Extras/vs.png");
    ui->loading->setPixmap(imagen);

    QMovie *movie = new QMovie(":/ImagenesJuego/Extras/LOADING-BLANCO.gif");
    QSize size(151,141); //x,y
    movie->setScaledSize(size);
    ui->gif->setMovie(movie);
    movie->start();

}

inicio::~inicio()
{
    delete ui;
}
