#ifndef OBSTACULOGRAF_H
#define OBSTACULOGRAF_H
#include <QPainter>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QTimer>
#include <QObject>
#include <iostream>

#include "obstaculofisico.h"
class obstaculoGraf: public QObject, public QGraphicsItem
{
public:
    //construcctor
    obstaculoGraf(float x, float y);
    ~obstaculoGraf();

    //metodo get-set
    obstaculoFisico *getItem();

    //Metodo simulacion
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void actualizar(float dt);
    void actualizarMoneda(float dt);

    //controlar animaciones
    void moverTortuga();
    void moverRasengan();
    void moverRayos();
    void moverEnemigoFinal();

    void moverVida();
    void moverMoneda();





    //Imagenes
    int imgTortuga;
    int imgRasengan;
    int imgRayos;
    int imgEnemigoFinal;

    int imgVida;
    int imgMoneda;


private slots:
    //sprites
    void tortuga();
    void girar();
    void rayo();
    void enemigofinal();

    void vida();
    void moneda();


private:

    int w, h;
    QTimer *timer;
    QPixmap pixmap;

    float px;
    float py;
    float vx;

    obstaculoFisico *obstaculo;



};

#endif // OBSTACULOGRAF_H
