#ifndef SELECCION_H
#define SELECCION_H

#include <QWidget>
#include <QMessageBox>

#include "juegoppal.h"

namespace Ui {
class Seleccion;
}

class Seleccion : public QWidget
{
    Q_OBJECT

public:
    explicit Seleccion(QWidget *parent = nullptr);
    ~Seleccion();
    void control();
    bool multijuador;
    QMediaPlayer* MusicSelec;




private slots:
    void on_Player1_toggled(bool checked);

    void on_Player2_toggled(bool checked);

    void on_Iniciar_clicked();

    void on_pushButton_clicked();

private:
    Ui::Seleccion *ui;
    JuegoPPAL *iniciar;

    QMediaPlayer* player;
    QMediaPlayer* click;
    QMediaPlayer *clickPPAL2;

    int p1=0,p2=0;
};

#endif // SELECCION_H
