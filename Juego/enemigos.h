#ifndef ENEMIGOS_H
#define ENEMIGOS_H

#include <QDialog>
#include <QMediaPlayer>

namespace Ui {
class Enemigos;
}

class Enemigos : public QDialog
{
    Q_OBJECT

public:
    explicit Enemigos(QWidget *parent = nullptr);
    ~Enemigos();


    int contador=2;

private slots:
    void on_pushButton_clicked();

    void on_BotonTort_clicked();

    void on_BotonRaseng_clicked();

    void on_BotonRayo_clicked();

    void on_BotonBoss_clicked();

private:
    Ui::Enemigos *ui;
    QMediaPlayer *Tortuga,*Rasengan,*rayo,*boss,*click;
};

#endif // ENEMIGOS_H
