#ifndef RESULTADOS_H
#define RESULTADOS_H

#include <QWidget>
#include <QPixmap>
#include <QTimer>
#include <QFile>
#include <iostream>
#include <QMovie>
#include <QSize>

namespace Ui {
class resultados;
}

class resultados : public QWidget
{
    Q_OBJECT

public:
    explicit resultados(QWidget *parent = nullptr);
    ~resultados();

    //atributos
    int op=0;
    QPixmap Imag;

    //metodos
    void imaganes();
    void reorganizar();
    void ocultar();
    void mostrar();
    void darvalores();


    bool multi=false;

private:
    Ui::resultados *ui;
    QMovie *mover;
};

#endif // RESULTADOS_H
