#include "enemigos.h"
#include "ui_enemigos.h"

Enemigos::Enemigos(QWidget *parent) :QDialog(parent),ui(new Ui::Enemigos)
{
    ui->setupUi(this);

    Tortuga= new QMediaPlayer(this);
    Tortuga->setMedia(QUrl("qrc:/MusicaJuego/Tortuga.mp3"));

    Rasengan= new QMediaPlayer(this);
    Rasengan->setMedia(QUrl("qrc:/MusicaJuego/rasengan.mp3"));

    rayo= new QMediaPlayer(this);
    rayo->setMedia(QUrl("qrc:/MusicaJuego/Rayo.mp3"));

    boss= new QMediaPlayer(this);
    boss->setMedia(QUrl("qrc:/MusicaJuego/Boss.mp3"));

    click= new QMediaPlayer(this);
    click->setMedia(QUrl("qrc:/MusicaJuego/click.mp3"));

    ui->text2->hide();
    ui->text3->hide();
    ui->text4->hide();

    ui->BotonRaseng->hide();
    ui->BotonRayo->hide();
    ui->BotonBoss->hide();


    ui->label_2->hide();
    ui->label_3->hide();
    ui->label_4->hide();




}

Enemigos::~Enemigos()
{
    delete ui;
    delete Tortuga;
    delete Rasengan;
    delete rayo;
    delete boss;
    delete click;
}

void Enemigos::on_pushButton_clicked()
{
    //this->close();
    click->play();
    if(contador==2){
        Tortuga->stop();

        ui->label_2->show();
        ui->BotonRaseng->show();
        ui->text2->show();
        ui->text2->setGeometry(20,10,521,371);
        ui->text1->hide();
        contador++;
    }
    else if(contador==3){
        Rasengan->stop();

        ui->label_3->show();
        ui->BotonRayo->show();
        ui->text3->show();
        ui->text3->setGeometry(20,10,521,371);
        ui->text2->hide();
        contador++;
    }
    else if (contador==4) {
        ui->label_4->show();
        ui->BotonBoss->show();
        ui->text4->show();
        ui->text4->setGeometry(20,10,521,371);
        ui->text3->hide();
        ui->pushButton->setText("Finalizar");
        contador++;
    }
    else if (contador==5) {
        boss->stop();

        this->close();
    }
}

void Enemigos::on_BotonTort_clicked()
{
    Tortuga->play();
}

void Enemigos::on_BotonRaseng_clicked()
{
    Rasengan->play();
}

void Enemigos::on_BotonRayo_clicked()
{
    rayo->play();
}

void Enemigos::on_BotonBoss_clicked()
{
    boss->play();
}
