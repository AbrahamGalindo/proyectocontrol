#ifndef CONTROL_ACCESO_H
#define CONTROL_ACCESO_H

#include <QWidget>
#include <QMainWindow>
#include <QPixmap>
#include <QPushButton>
#include <QTimer>
#include <iostream>
#include "menu.h"

#include <QMediaPlayer>

namespace Ui {
class control_acceso;
}

class control_acceso : public QWidget
{
    Q_OBJECT

public:
    explicit control_acceso(QWidget *parent = nullptr);
    ~control_acceso();


    //atributos
    int correctas=0;
    int res=0;
    int preg=1;
    int OK=1;
    QPixmap Imag_control;
    QTimer *fin;

    //metodos
    void control();
    void ocultar();
    void reordenar(int op1_y, int op2_y , int op3_y, int op4_y);

private slots:
    void on_Iniciar_clicked();
    void final();


    void on_cerrar_clicked();

private:
    Ui::control_acceso *ui;
    Menu *menuI;

    QMediaPlayer* player;
    QMediaPlayer* click;
};

#endif // CONTROL_ACCESO_H
