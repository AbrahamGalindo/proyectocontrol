#ifndef PERSONAJEFISICO_H
#define PERSONAJEFISICO_H


class personajeFisico
{
public:
    personajeFisico(float x, float y);
    void actualizar(float dt);

    float getPx() const;
    void setPx(float value);

    float getPy() const;
    void setPy(float value);

    float getVx() const;
    void setVx(float value);

    float getVy() const;
    void setVy(float value);

private:
    float px,py,vx,vy;
};

#endif // PERSONAJEFISICO_H
