#include "control_acceso.h"
#include "ui_control_acceso.h"

control_acceso::control_acceso(QWidget *parent) :QWidget(parent),ui(new Ui::control_acceso)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);//quitar los bordes y botones
    this->setAttribute(Qt::WA_TranslucentBackground);//ponerla transparente

    player=new QMediaPlayer(this);
    player->setMedia(QUrl("qrc:/MusicaJuego/ControlAcceso-Menu.mp3"));
    player->setVolume(30);
    player->play();

    click= new QMediaPlayer(this);
    click->setMedia(QUrl("qrc:/MusicaJuego/click.mp3"));
    click->setVolume(100);

    menuI = new Menu();
    ui->Op1->hide();
    ui->Op2->hide();
    ui->Op3->hide();
    ui->Op4->hide();

    fin = new QTimer(this);
    fin->stop();

    connect(fin,SIGNAL(timeout()),this,SLOT(final()));
    ui->line_1->move(49,5); //x,y
    ui->line_2->move(1046,10);
    ui->line_3->move(50,515);
    ui->line_4->move(36,10);
}

control_acceso::~control_acceso()
{
    delete ui;
    delete menuI;
    delete fin;
    delete player;
    delete click;
}

void control_acceso::reordenar(int op1_y, int op2_y , int op3_y, int op4_y)
{
    ui->Op1->move(200,op1_y);
    ui->Op2->move(200,op2_y);
    ui->Op3->move(200,op3_y);
    ui->Op4->move(200,op4_y);
}

void control_acceso::control()
{
    ui->Op1->show();
    ui->Op2->show();
    ui->Op3->show();
    ui->Op4->show();

    if(ui->Op1->isChecked()){res=1;}
    if(ui->Op2->isChecked()){res=2;}
    if(ui->Op3->isChecked()){res=3;}
    if(ui->Op4->isChecked()){res=4;}

    if(preg==1){
        Imag_control.load(":/ImagenesJuego/control de acceso/pregunta1.png");
        ui->Control_acceso->setPixmap(Imag_control);

        ui->Op1->move(86,220); //x,y
        ui->Op2->move(86,292);
        ui->Op3->move(86,370);
        ui->Op4->move(86,440);

    }

    if(preg==2){
        Imag_control.load(":/ImagenesJuego/control de acceso/pregunta2.png");
        ui->Control_acceso->setPixmap(Imag_control);
        ui->Op1->move(91,224); //x,y
        ui->Op2->move(91,302);
        ui->Op3->move(91,382);
        ui->Op4->move(91,460);


        if(res==4){correctas++;}

    }

    if(preg==3){
        Imag_control.load(":/ImagenesJuego/control de acceso/pregunta3.png");
        ui->Control_acceso->setPixmap(Imag_control);
        ui->Op1->move(91,230); //x,y
        ui->Op2->move(91,305);
        ui->Op3->move(91,392);
        ui->Op4->move(91,470);


        if(res==2){correctas++;}
    }

    if(preg==4){
        Imag_control.load(":/ImagenesJuego/control de acceso/pregunta4.png");
        ui->Control_acceso->setPixmap(Imag_control);
        ui->Op1->move(80,233); //x,y
        ui->Op2->move(80,305);
        ui->Op3->move(80,389);
        ui->Op4->move(80,465);

        if(res==2){correctas++;}


    }
    if(preg==5){
        Imag_control.load(":/ImagenesJuego/control de acceso/pregunta5.png");
        ui->Control_acceso->setPixmap(Imag_control);
        ui->Op1->move(75,229); //x,y
        ui->Op2->move(75,308);
        ui->Op3->move(75,395);
        ui->Op4->move(75,465);

        if(res==1){correctas++;}
    }

    if(preg==6){
        ocultar();
        if(res==1){correctas++;}

        if(correctas>3){
            OK=1;
            Imag_control.load(":/ImagenesJuego/control de acceso/ganaste.png");
            ui->Control_acceso->setPixmap(Imag_control);
        }
        else {
            OK=0;
            Imag_control.load(":/ImagenesJuego/control de acceso/fallaste.png");
            ui->Control_acceso->setPixmap(Imag_control);
        }
        preg++;
    }

    if(preg==7){
        fin->start(4000);
    }
    preg++;
}

void control_acceso::ocultar()
{
    ui->Op1->hide();
    ui->Op2->hide();
    ui->Op3->hide();
    ui->Op4->hide();

    ui->Iniciar->hide();
}



void control_acceso::on_Iniciar_clicked()
{
    click->play();
    ui->Iniciar->setText("Siguiente");
    control();
}

void control_acceso::final()
{
    if(OK==1){
        this->close();
        menuI->showMaximized();
        fin->stop();
        player->stop();


    }

    if(OK==0){
        this->close();
        fin->stop();
    }
}

void control_acceso::on_cerrar_clicked()
{
    click->play();
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this,"Cerrar","¿Seguro que quieres salir del control de acceso?",
                                  QMessageBox::Yes|QMessageBox::No);
    if(reply==QMessageBox::Yes){
        this->close();
    }
}
