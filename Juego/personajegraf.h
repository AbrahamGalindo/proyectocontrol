#ifndef PERSONAJEGRAF_H
#define PERSONAJEGRAF_H

#include <QPainter>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QTimer>
#include <QObject>
#include "personajefisico.h"

class personajegraf: public QObject, public QGraphicsItem
{
public:
    personajegraf(float _x,float _y);
    void actualizar(float dt);
    ~personajegraf();


    personajeFisico *getPersonaje() const;
    void setPersonaje(personajeFisico *value);

public slots:
    void volar1();
    void volar2();
    void potenciado();
    void choque1();
    void choque2();
    void muerte();
    void muerte2();
    void muerte3();
    void muerte4();

private:

    QTimer *imagenVolar1;
    QTimer *imagenVolar2;
    QTimer *imagenChoque1;
    QTimer *imagenChoque2;
    QTimer *imagenMuerte;
    QTimer *imagenMuerte2;
    QTimer *imagenMuerte3;
    QTimer *imagenMuerte4;

    int imgVolar=1;
    int imgVolar2=1;
    int imgChoque1=1;
    int imgChoque2=1;
    int imgMuerte=1;
    int imgMuerte2=1;
    int imgMuerte3=1;
    int imgMuerte4=1;

    int w, h;
    QPixmap pixmap;
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    personajeFisico *personaje;
    void stopTimer();


};

#endif // PERSONAJEGRAF_H

