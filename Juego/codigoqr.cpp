#include "codigoqr.h"
#include "ui_codigoqr.h"

CodigoQR::CodigoQR(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CodigoQR)
{
    ui->setupUi(this);
}

CodigoQR::~CodigoQR()
{
    delete ui;
}

void CodigoQR::on_pushButton_clicked()
{
    this->close();
}
