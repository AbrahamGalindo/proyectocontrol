#include "personajefisico.h"

personajeFisico::personajeFisico(float x, float y)
{
    vx=75;
    px=x;
    py=y;

}

void personajeFisico::actualizar(float dt)
{
    px+=vx*dt;
}

float personajeFisico::getPx() const
{
    return px;
}

void personajeFisico::setPx(float value)
{
    px = value;
}

float personajeFisico::getPy() const
{
    return py;
}

void personajeFisico::setPy(float value)
{
    py = value;
}

float personajeFisico::getVx() const
{
    return vx;
}

void personajeFisico::setVx(float value)
{
    vx = value;
}

float personajeFisico::getVy() const
{
    return vy;
}

void personajeFisico::setVy(float value)
{
    vy = value;
}
