#-------------------------------------------------
#
# Project created by QtCreator 2018-10-25T09:53:02
#
#-------------------------------------------------

QT       += core gui multimedia  serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = juegoPate1
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        juegoppal.cpp \
    personajegraf.cpp \
    personajefisico.cpp \
    obstaculograf.cpp \
    obstaculofisico.cpp \
    menu.cpp \
    control_acceso.cpp \
    resultados.cpp \
    inicio.cpp \
    seleccion.cpp \
    codigoqr.cpp \
    enemigos.cpp \
    instrucciones.cpp

HEADERS += \
        juegoppal.h \
    personajegraf.h \
    personajefisico.h \
    obstaculograf.h \
    obstaculofisico.h \
    menu.h \
    control_acceso.h \
    resultados.h \
    inicio.h \
    seleccion.h \
    codigoqr.h \
    enemigos.h \
    instrucciones.h

FORMS += \
        juegoppal.ui \
    menu.ui \
    control_acceso.ui \
    resultados.ui \
    inicio.ui \
    seleccion.ui \
    codigoqr.ui \
    enemigos.ui \
    instrucciones.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    ../build-juegoPate1-Desktop_Qt_5_11_2_MinGW_32bit-Debug/Tree.png \
    Fondo/Fondo Juego.png \
    Fondo/Fondo Juego.png

RESOURCES += \
    imagenes.qrc \
    musica.qrc
