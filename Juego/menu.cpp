#include "menu.h"
#include "ui_menu.h"

Menu::Menu(QWidget *parent) :QMainWindow(parent),ui(new Ui::Menu)
{
    ui->setupUi(this);
    iniciar=new JuegoPPAL();
    selec=new Seleccion();

    click=new QMediaPlayer(this);
    click2=new QMediaPlayer(this);
    click3=new QMediaPlayer(this);

    click->setMedia(QUrl("qrc:/MusicaJuego/click.mp3"));
    click->setVolume(100);

    click2->setMedia(QUrl("qrc:/MusicaJuego/error.mp3"));
    click2->setVolume(100);

    click3->setMedia(QUrl("qrc:/MusicaJuego/correcto.mp3"));
    click3->setVolume(100);


}

Menu::~Menu()
{
    delete ui;
    delete iniciar;
    delete selec;
    delete click;
    delete click2;
    delete click3;


}


void Menu::on_UnJugador_clicked()
{
    click->play();
    selec->multijuador=false;
    selec->MusicSelec->play();
    selec->show();

    this->close();
}

void Menu::on_Multijugador_clicked()
{
    click->play();
    iniciar->jugador2=false;
    iniciar->Multiplayer=true;

    iniciar->showMaximized();
    iniciar->setDosjugadores(true);

    this->close();
}






void Menu::on_pushButton_clicked()
{
    click->play();
    iniciar->cargar=true;
    QFile archivo;
    QTextStream io;
    QString nombreArch,infoGuardar,filter = "Text File (*.txt)";
    nombreArch=QFileDialog::getOpenFileName(this,"Cargar Partida","C://Users//Fafoa//Desktop//Juego//Partidas Guardadas"); //this,nombre,directorio
    archivo.setFileName(nombreArch);
    archivo.open(QIODevice::ReadOnly | QIODevice::Text);
    if(!archivo.isOpen()){
        click2->play();
        QMessageBox::critical(this,"ERROR","No se ha seleccionado ninguna partida!");

    }
    else {
        infoGuardar=archivo.readLine();
        QList<QString> contenedor;
        int n=0;
        while(n>=0){
            n=infoGuardar.indexOf("\t");
            if(n!=0){
                contenedor.append(infoGuardar.left(n));
            }
            infoGuardar=infoGuardar.remove(0,n+1);
        }
        QList<float> cargarPerson;
        QList<float> cargarTortuga;
        QList<float> cargarRasengan;
        QList<float> cargarRayos;
        QList<float> cargarMonedas;
        QList<float> cargarVidas;
        QList<float> cargarEnemi;

        for(int i=0,posicion=1;i<contenedor.size();i++){
            if(contenedor.at(i)=="|"){
                posicion++;
                continue;
            }
            switch (posicion) {
            case 1:
                cargarPerson.push_back(contenedor.at(i).toFloat());
                break;
            case 2:
                cargarTortuga.push_back(contenedor.at(i).toFloat());
                break;
            case 3:
                cargarRasengan.push_back(contenedor.at(i).toFloat());
                break;
            case 4:
                cargarRayos.push_back(contenedor.at(i).toFloat());
                break;
            case 5:
                cargarMonedas.push_back(contenedor.at(i).toFloat());
                break;
            case 6:
                cargarVidas.push_back(contenedor.at(i).toFloat());
                break;
            case 7:
                cargarEnemi.push_back(contenedor.at(i).toFloat());
            }

        }
        for(int i=0;i<cargarPerson.size();i++){
            switch (i) {
            case 0:
                iniciar->posX=cargarPerson.at(0);
                break;
            case 1:
                iniciar->posY=cargarPerson.at(1);
                break;
            case 2:
                iniciar->contadorVidas=(int)cargarPerson.at(3);
                break;
            case 3:
                iniciar->distanciaLCD=(int)cargarPerson.at(4);
                break;
            case 4:
                iniciar->MonedasLCD=(int)cargarPerson.at(5);
                break;
            case 5:
                iniciar->jugador2=(int)cargarPerson.at(6);
                break;
            }
        }

        iniciar->tort=cargarTortuga;
        iniciar->raseng=cargarRasengan;
        iniciar->ray=cargarRayos;
        iniciar->mon=cargarMonedas;
        iniciar->vid=cargarVidas;
        iniciar->enemi=cargarEnemi;

        iniciar->showMaximized();
        iniciar->setDosjugadores(false);
        this->close();
        click3->play();
        QMessageBox::information(this,tr("Cargando"),tr("Partida cargada con exito!"));
    }

}

void Menu::on_actionEnemigos_triggered()
{

    click->play();
    Enemigos *ventana2 = new Enemigos(this);
    ventana2->show();
}



void Menu::on_pushButton_2_clicked()
{
    click->play();
    CodigoQR *Ventana1= new CodigoQR(this);
    Ventana1->show();
}

void Menu::on_actionInstrucciones_triggered()
{
    click->play();
    instrucciones *ventana = new instrucciones(this);
    ventana->show();
}
