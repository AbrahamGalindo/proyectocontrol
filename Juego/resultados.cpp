#include "resultados.h"
#include "ui_resultados.h"
#include <QDebug>

resultados::resultados(QWidget *parent) :QWidget(parent),ui(new Ui::resultados)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);//quitar los bordes y botones
    this->setAttribute(Qt::WA_TranslucentBackground);//ponerla transparente

    ui->line_1->move(40,56); //x,y
    ui->line_2->move(1320,60);
    ui->line_3->move(39,647);
    ui->line_4->move(28,60);
}

resultados::~resultados()
{
    delete ui;
}
//metodos
void resultados::imaganes()
{
    if(op==1){
        mostrar();
        ocultar();
        reorganizar();
        darvalores();

        Imag.load(":/ImagenesJuego/Resultados/PerdioTortP1.png");
        ui->fondo->setPixmap(Imag);

        if(multi==false){
            QMovie *mover = new QMovie(":/ImagenesJuego/Resultados/GifP1.gif");
            QSize size(320,35); //x,y
            mover->setScaledSize(size);
            ui->gif->move(1000,596);
            ui->gif->setMovie(mover);
            ui->gif->show();
            mover->start();
        }
        else if (multi==true) {
        }
    }


    if(op==2){ //perdio personaje 1 con rasengan
        mostrar();
        ocultar();
        darvalores();
        reorganizar();

        Imag.load(":/ImagenesJuego/Resultados/PerdioRasengP1.png");
        ui->fondo->setPixmap(Imag);

        if(multi==false){
            QMovie *mover = new QMovie(":/ImagenesJuego/Resultados/GifP1.gif");
            QSize size(320,35); //x,y
            mover->setScaledSize(size);
            ui->gif->move(1000,596);
            ui->gif->setMovie(mover);
            ui->gif->show();
            mover->start();
        }
        else if (multi==true) {
        }
    }
    if(op==3){ //perdio personaje 1 con rayo
        mostrar();
        ocultar();
        darvalores();
        reorganizar();

        Imag.load(":/ImagenesJuego/Resultados/PerdioRayoP1.png");
        ui->fondo->setPixmap(Imag);

        if(multi==false){
            QMovie *mover = new QMovie(":/ImagenesJuego/Resultados/GifP1.gif");
            QSize size(320,35); //x,y
            mover->setScaledSize(size);
            ui->gif->move(1000,596);
            ui->gif->setMovie(mover);
            ui->gif->show();
            mover->start();
        }
        else if (multi==true) {
        }
    }
    if(op==4){ //perdio personaje 2 con tortuga
        mostrar();
        ocultar();
        darvalores();
        reorganizar();

        Imag.load(":/ImagenesJuego/Resultados/PerdioTortP2.png");
        ui->fondo->setPixmap(Imag);

        if(multi==false){
            QMovie *mover = new QMovie(":/ImagenesJuego/Resultados/GifP2.gif");
            QSize size(320,35); //x,y
            mover->setScaledSize(size);
            ui->gif->move(1000,596);
            ui->gif->setMovie(mover);
            ui->gif->show();
            mover->start();
        }
        else if (multi==true) {
        }
    }
    if(op==5){ //perdio personaje 2 con rasengan
        mostrar();
        ocultar();
        darvalores();
        reorganizar();

        Imag.load(":/ImagenesJuego/Resultados/PerdioRasengP2.png");
        ui->fondo->setPixmap(Imag);

        if(multi==false){
            QMovie *mover = new QMovie(":/ImagenesJuego/Resultados/GifP2.gif");
            QSize size(320,35); //x,y
            mover->setScaledSize(size);
            ui->gif->move(1000,596);
            ui->gif->setMovie(mover);
            ui->gif->show();
            mover->start();
        }
        else if (multi==true) {
        }
    }
    if(op==6){ //perdio personaje 2 con rayo
        mostrar();
        ocultar();
        darvalores();
        reorganizar();

        Imag.load(":/ImagenesJuego/Resultados/PerdioRayoP2.png");
        ui->fondo->setPixmap(Imag);

        if(multi==false){
            QMovie *mover = new QMovie(":/ImagenesJuego/Resultados/GifP2.gif");
            QSize size(320,35); //x,y
            mover->setScaledSize(size);
            ui->gif->move(1000,596);
            ui->gif->setMovie(mover);
            ui->gif->show();
            mover->start();
        }
        else if (multi==true) {
        }
    }
    if(op==7){ //Gano personaje 1
        mostrar();
        ocultar();
        darvalores();
        reorganizar();


        Imag.load(":/ImagenesJuego/Resultados/Ganador1.png");
        ui->fondo->setPixmap(Imag);
    }

    if(op==8){ //Gano personaje 2
        mostrar();
        ocultar();
        darvalores();
        reorganizar();


        Imag.load(":/ImagenesJuego/Resultados/Ganador2.png");
        ui->fondo->setPixmap(Imag);
    }
    if(op==9){ //Gano personaje 1
        mostrar();
        ocultar();
        darvalores();
        reorganizar();


        Imag.load(":/ImagenesJuego/Resultados/GanoP1.png");
        ui->fondo->setPixmap(Imag);
    }
    if(op==10){ //Gano personaje 2
        mostrar();
        ocultar();
        darvalores();
        reorganizar();


        Imag.load(":/ImagenesJuego/Resultados/GanoP2.png");
        ui->fondo->setPixmap(Imag);
    }


    if(op==11){

        mostrar();
        ocultar();
        reorganizar();
        darvalores();

        Imag.load(":/ImagenesJuego/Resultados/PerdioBossP1.png");
        ui->fondo->setPixmap(Imag);

        if(multi==false){
            QMovie *mover = new QMovie(":/ImagenesJuego/Resultados/GifP1.gif");
            QSize size(320,35); //x,y
            mover->setScaledSize(size);
            ui->gif->move(1000,596);
            ui->gif->setMovie(mover);
            ui->gif->show();
            mover->start();
        }
        else if (multi==true) {

        }
    }

    if(op==12){
        mostrar();
        ocultar();
        darvalores();
        reorganizar();

        Imag.load(":/ImagenesJuego/Resultados/PerdioBossP2.png");
        ui->fondo->setPixmap(Imag);

        if(multi==false){
            QMovie *mover = new QMovie(":/ImagenesJuego/Resultados/GifP2.gif");
            QSize size(320,35); //x,y
            mover->setScaledSize(size);
            ui->gif->move(1000,596);
            ui->gif->setMovie(mover);
            ui->gif->show();
            mover->start();
        }
        else if (multi==true) {

        }
    }



}


void resultados::reorganizar()
{
   if(op==1){
       ui->Distancia->move(1125,272);
       ui->Monedas->move(1130,375);
       ui->Total->move(1120,469);
   }


    if(op==2){
        ui->Distancia->move(1125,272); //x,y
        ui->Monedas->move(1130,375);
        ui->Total->move(1120,469);
    }

    if(op==3){
        ui->Distancia->move(1125,272); //x,y
        ui->Monedas->move(1130,375);
        ui->Total->move(1120,469);
    }

    if(op==4){
        ui->Distancia_2->move(1095,268);
        ui->Monedas_2->move(1095,371);
        ui->Total_2->move(1075,464);
    }

    if(op==5){
        ui->Distancia_2->move(1095,268);
        ui->Monedas_2->move(1095,371);
        ui->Total_2->move(1075,464);
    }

    if(op==6){
        ui->Distancia_2->move(1095,268);
        ui->Monedas_2->move(1095,371);
        ui->Total_2->move(1075,464);
    }

    //multijugador
    if(op==7){

        ui->Total->move(695,434);
    }
    if(op==8){

        ui->Total_2->move(695,434);
    }

    if(op==9){
        ui->Distancia->move(1125,260);
        ui->Monedas->move(1130,388);
        ui->Total->move(1088,500);
    }
    if(op==10){
        ui->Distancia_2->move(1092,261);
        ui->Monedas_2->move(1092,363);
        ui->Total_2->move(1088,498);
    }

    if(op==11){
        ui->Distancia->move(1125,272); //x,y
        ui->Monedas->move(1130,375);
        ui->Total->move(1120,469);
    }
    if(op==12){
        ui->Distancia_2->move(1095,268);
        ui->Monedas_2->move(1095,371);
        ui->Total_2->move(1075,464);
    }
}

void resultados::ocultar()
{
    if(op==1){
        ui->Distancia_2->hide();
        ui->Monedas_2->hide();
        ui->Total_2->hide();

    }
    if(op==2){
        ui->Distancia_2->hide();
        ui->Monedas_2->hide();
        ui->Total_2->hide();
    }
    if(op==3){
        ui->Distancia_2->hide();
        ui->Monedas_2->hide();
        ui->Total_2->hide();
    }
    if(op==4){
        ui->Distancia->hide();
        ui->Monedas->hide();
        ui->Total->hide();
    }

    if(op==5){
        ui->Distancia->hide();
        ui->Monedas->hide();
        ui->Total->hide();
    }

    if(op==6){
        ui->Distancia->hide();
        ui->Monedas->hide();
        ui->Total->hide();
    }

    //multijugador
    if(op==7){
        ui->Distancia->hide();
        ui->Monedas->hide();

        ui->Distancia_2->hide();
        ui->Monedas_2->hide();
        ui->Total_2->hide();
    }
    if(op==8){
        ui->Distancia->hide();
        ui->Monedas->hide();
        ui->Total->hide();

        ui->Distancia_2->hide();
        ui->Monedas_2->hide();
    }
    if(op==9){
        ui->Distancia_2->hide();
        ui->Monedas_2->hide();
        ui->Total_2->hide();
    }
    if(op==10){
        ui->Distancia->hide();
        ui->Monedas->hide();
        ui->Total->hide();
    }

    if(op==11){
        ui->Distancia_2->hide();
        ui->Monedas_2->hide();
        ui->Total_2->hide();
    }

    if(op==12){
        ui->Distancia->hide();
        ui->Monedas->hide();
        ui->Total->hide();
    }
}

void resultados::mostrar()
{
    ui->Distancia->show();
    ui->Monedas->show();
    ui->Total->show();

    ui->Distancia_2->show();
    ui->Monedas_2->show();
    ui->Total_2->show();
}

void resultados::darvalores()
{
    QString dato;
    QFile file("Guardar.txt");
    file.open(QIODevice::ReadOnly);
    dato=file.readLine();

    QList<QString> valores;
    int n=0;
    while(n>=0){
        n = dato.indexOf("\t");
        if(n!=0){
            valores.append(dato.left(n));
        }
        dato=dato.remove(0,n+1);
    }
    file.close();

    int distancia=0,monedas=0,total=0;

    distancia=valores.at(4).toInt();
    monedas=valores.at(5).toInt();
    total=distancia*2+(monedas*8);

    if(op==1){
        ui->Distancia->setText(QString::number(distancia));
        ui->Monedas->setText(QString::number(monedas));
        ui->Total->setText(QString::number(total));
    }
    else if(op==2){
        ui->Distancia->setText(QString::number(distancia));
        ui->Monedas->setText(QString::number(monedas));
        ui->Total->setText(QString::number(total));

    }
    else if (op==3) {
        ui->Distancia->setText(QString::number(distancia));
        ui->Monedas->setText(QString::number(monedas));
        ui->Total->setText(QString::number(total));
    }

    else if(op==4){
        ui->Distancia_2->setText(QString::number(distancia));
        ui->Monedas_2->setText(QString::number(monedas));
        ui->Total_2->setText(QString::number(total));

    }

    else if(op==5){
        ui->Distancia_2->setText(QString::number(distancia));
        ui->Monedas_2->setText(QString::number(monedas));
        ui->Total_2->setText(QString::number(total));

    }

    else if(op==6){
        ui->Distancia_2->setText(QString::number(distancia));
        ui->Monedas_2->setText(QString::number(monedas));
        ui->Total_2->setText(QString::number(total));

    }


//    //multijugador
    else if(op==7){
        ui->Total->setText(QString::number(total));
    }
    else if(op==8){
         ui->Total_2->setText(QString::number(total));
    }

    else if (op==9) {
        ui->Distancia->setText(QString::number(distancia));
        ui->Monedas->setText(QString::number(monedas));
        ui->Total->setText(QString::number(total));
    }

    else if(op==10){
        ui->Distancia_2->setText(QString::number(distancia));
        ui->Monedas_2->setText(QString::number(monedas));
        ui->Total_2->setText(QString::number(total));
    }
    else if (op==11) {
        ui->Distancia->setText(QString::number(distancia));
        ui->Monedas->setText(QString::number(monedas));
        ui->Total->setText(QString::number(total));
    }
    else if(op==12){
        ui->Distancia_2->setText(QString::number(distancia));
        ui->Monedas_2->setText(QString::number(monedas));
        ui->Total_2->setText(QString::number(total));
    }

}


