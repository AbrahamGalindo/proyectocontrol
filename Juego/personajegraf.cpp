#include "personajegraf.h"

personajegraf::personajegraf(float _x, float _y)
{
    setPos(_x,_y);
    personaje=new personajeFisico(_x,_y);

    imagenVolar1= new QTimer();
    imagenVolar2= new QTimer();
    imagenChoque1= new QTimer();
    imagenChoque2= new QTimer();
    imagenMuerte= new QTimer();
    imagenMuerte2= new QTimer();
    imagenMuerte3= new QTimer();
    imagenMuerte4= new QTimer();




    connect(imagenVolar1,&QTimer::timeout,this,&personajegraf::volar1);
    connect(imagenVolar2,&QTimer::timeout,this,&personajegraf::volar2);
    connect(imagenChoque1,&QTimer::timeout,this,&personajegraf::choque1);
    connect(imagenChoque2,&QTimer::timeout,this,&personajegraf::choque2);
    connect(imagenMuerte,&QTimer::timeout,this,&personajegraf::muerte);
    connect(imagenMuerte2,&QTimer::timeout,this,&personajegraf::muerte2);
    connect(imagenMuerte3,&QTimer::timeout,this,&personajegraf::muerte3);
    connect(imagenMuerte4,&QTimer::timeout,this,&personajegraf::muerte4);


}


QRectF personajegraf::boundingRect() const
{
    return QRect(0,0,w,h);
}

void personajegraf::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

    painter->drawPixmap(boundingRect(),pixmap,pixmap.rect());

}

void personajegraf::stopTimer()
{
    imagenVolar1->stop();
    imagenVolar2->stop();
    imagenChoque1->stop();
    imagenChoque2->stop();
    imagenMuerte->stop();
    imagenMuerte2->stop();
    imagenMuerte3->stop();
    imagenMuerte4->stop();



}

personajeFisico *personajegraf::getPersonaje() const
{
    return personaje;
}

void personajegraf::setPersonaje(personajeFisico *value)
{
    personaje = value;
}

void personajegraf::actualizar(float dt)
{
    personaje->actualizar(dt);
    setPos(personaje->getPx(),personaje->getPy());   //setPos donde lo fisco se vuelve grafico att:juan josexdxd

}
personajegraf::~personajegraf()
{
    delete personaje;
    delete imagenChoque1;
    delete imagenChoque2;
    delete imagenVolar1;
    delete imagenVolar2;
    delete imagenMuerte;
    delete imagenMuerte2;
    delete imagenMuerte3;
    delete imagenMuerte4;
}

void personajegraf::volar1()
{
    //w: largo, h: ancho

    stopTimer();
    imagenVolar1->start(100);
    switch (imgVolar) {
    case 1:
        pixmap.load(":/ImagenesJuego/Volando/1.png");
        w=146; h=93;
        imgVolar++;
        break;
    case 2:
        pixmap.load(":/ImagenesJuego/Volando/2.png");
        w=146; h=93;
        imgVolar++;
        break;
    case 3:
        pixmap.load(":/ImagenesJuego/Volando/3.png");
        w=146; h=93;
        imgVolar++;
        break;
    case 4:
        pixmap.load(":/ImagenesJuego/Volando/4.png");
        w=146; h=93;
        imgVolar=1;
        break;

    }
}

void personajegraf::volar2()
{
    stopTimer();
    imagenVolar2->start(100);
    switch (imgVolar2) {
    case 1:
        pixmap.load(":/ImagenesJuego/Volando2/Volar1.png");
        w=146; h=100;
        imgVolar2++;
        break;
    case 2:
        pixmap.load(":/ImagenesJuego/Volando2/Volar2.png");
        w=146; h=100;
        imgVolar2++;
        break;
    case 3:
        pixmap.load(":/ImagenesJuego/Volando2/Volar3.png");
        w=146; h=100;
        imgVolar2=1;
        break;

    }
}

void personajegraf::choque1()
{
    stopTimer();
    imagenChoque1->start(100);
    switch (imgChoque1) {
    case 1:
        pixmap.load(":/ImagenesJuego/Herido/Herido1.png");
        w=146; h=93;
        imgChoque1++;
        break;
    case 2:
        pixmap.load(":/ImagenesJuego/Herido/Herido2.png");
        w=146; h=93;
        imgChoque1++;
        break;
    case 3:
        pixmap.load(":/ImagenesJuego/Herido/Herido3.png");
        w=146; h=93;
        imgChoque1++;
        break;
    case 4:
        pixmap.load(":/ImagenesJuego/Herido/Herido4.png");
        w=146; h=93;
        imgChoque1=1;
        break;
    }
}

void personajegraf::choque2()
{
    stopTimer();
    imagenChoque2->start(100);
    switch (imgChoque2) {
    case 1:
        pixmap.load(":/ImagenesJuego/Herido2/Herido11.png");
        w=146;h=100;
        imgChoque2++;
        break;
    case 2:
        pixmap.load(":/ImagenesJuego/Herido2/Herido12.png");
        w=146;h=100;
        imgChoque2++;
        break;
    case 3:
        pixmap.load(":/ImagenesJuego/Herido2/Herido13.png");
        w=146;h=100;
        imgChoque2=1;
        break;
    }
}

void personajegraf::muerte()
{
    stopTimer();
    imagenMuerte->start(100);
    switch (imgMuerte) {
    case 1:
        pixmap.load(":/ImagenesJuego/Muerte/1.png");
        w=67; h=68;
        imgMuerte++;
        break;
    case 2:
        pixmap.load(":/ImagenesJuego/Muerte/2.png");
        w=98; h=98;
        imgMuerte++;
        break;
    case 3:
        pixmap.load(":/ImagenesJuego/Muerte/3.png");
        w=134; h=121;
        imgMuerte++;
        break;
    case 4:
        pixmap.load(":/ImagenesJuego/Muerte/4.png");
        w=134; h=121;
        imgMuerte++;
        break;
    case 5:
        pixmap.load(":/ImagenesJuego/Muerte/5.png");
        w=138; h=119;
        imgMuerte++;
        break;
    case 6:
        pixmap.load(":/ImagenesJuego/Muerte/6.png");
        w=138; h=119;
        imgMuerte++;
        break;
    case 7:
        pixmap.load(":/ImagenesJuego/Muerte/7.png");
        w=122; h=117;
        imgMuerte++;
        break;
    case 8:
        pixmap.load(":/ImagenesJuego/Muerte/8.png");
        w=122; h=117;
        imgMuerte++;
        break;
    case 9:
        pixmap.load(":/ImagenesJuego/Muerte/9.png");
        w=97; h=102;
        imgMuerte++;
        break;
    case 10:
        pixmap.load(":/ImagenesJuego/Muerte/10.png");
        w=97; h=102;
        imgMuerte++;
        break;
    case 11:
        pixmap.load(":/ImagenesJuego/Muerte/11.png");
        w=56; h=71;
        imgMuerte++;
        break;
    case 12:
        pixmap.load(":/ImagenesJuego/Muerte/12.png");
        w=56; h=71;
        imgMuerte++;
        break;
    }
}

void personajegraf::muerte2()
{
    stopTimer();
    imagenMuerte2->start(100);
    switch (imgMuerte2) {
    case 1:
        pixmap.load(":/ImagenesJuego/Muerte2/1.png");
        w=66; h=67;
        imgMuerte2++;
        break;
    case 2:
        pixmap.load(":/ImagenesJuego/Muerte2/2.png");
        w=96; h=97;
        imgMuerte2++;
        break;
    case 3:
        pixmap.load(":/ImagenesJuego/Muerte2/3.png");
        w=132; h=119;
        imgMuerte2++;
        break;
    case 4:
        pixmap.load(":/ImagenesJuego/Muerte2/4.png");
        w=132; h=119;
        imgMuerte2++;
        break;
    case 5:
        pixmap.load(":/ImagenesJuego/Muerte2/5.png");
        w=136; h=117;
        imgMuerte2++;
        break;
    case 6:
        pixmap.load(":/ImagenesJuego/Muerte2/6.png");
        w=136; h=117;
        imgMuerte2++;
        break;
    case 7:
        pixmap.load(":/ImagenesJuego/Muerte2/7.png");
        w=120; h=115;
        imgMuerte2++;
        break;
    case 8:
        pixmap.load(":/ImagenesJuego/Muerte2/8.png");
        w=120; h=115;
        imgMuerte2++;
        break;
    case 9:
        pixmap.load(":/ImagenesJuego/Muerte2/9.png");
        w=96; h=100;
        imgMuerte2++;
        break;
    case 10:
        pixmap.load(":/ImagenesJuego/Muerte2/10.png");
        w=96; h=100;
        imgMuerte2++;
        break;
    case 11:
        pixmap.load(":/ImagenesJuego/Muerte2/11.png");
        w=55; h=70;
        imgMuerte2++;
        break;
    case 12:
        pixmap.load(":/ImagenesJuego/Muerte2/12.png");
        w=55; h=70;
        imgMuerte2++;
        break;
    }
}

void personajegraf::muerte3()
{
    stopTimer();
    imagenMuerte3->start(100);
    switch (imgMuerte3) {
    case 1:
        pixmap.load(":/ImagenesJuego/Muerte3/1.png");
        w=67; h=68;
        imgMuerte3++;
        break;
    case 2:
        pixmap.load(":/ImagenesJuego/Muerte3/2.png");
        w=98; h=98;
        imgMuerte3++;
        break;
    case 3:
        pixmap.load(":/ImagenesJuego/Muerte3/3.png");
        w=134; h=121;
        imgMuerte3++;
        break;
    case 4:
        pixmap.load(":/ImagenesJuego/Muerte3/4.png");
        w=134; h=121;
        imgMuerte3++;
        break;
    case 5:
        pixmap.load(":/ImagenesJuego/Muerte3/5.png");
        w=138; h=119;
        imgMuerte3++;
        break;
    case 6:
        pixmap.load(":/ImagenesJuego/Muerte3/6.png");
        w=138; h=119;
        imgMuerte3++;
        break;
    case 7:
        pixmap.load(":/ImagenesJuego/Muerte3/7.png");
        w=122; h=117;
        imgMuerte3++;
        break;
    case 8:
        pixmap.load(":/ImagenesJuego/Muerte3/8.png");
        w=122; h=117;
        imgMuerte3++;
        break;
    case 9:
        pixmap.load(":/ImagenesJuego/Muerte3/9.png");
        w=97; h=102;
        imgMuerte3++;
        break;
    case 10:
        pixmap.load(":/ImagenesJuego/Muerte3/10.png");
        w=97; h=102;
        imgMuerte3++;
        break;
    case 11:
        pixmap.load(":/ImagenesJuego/Muerte3/11.png");
        w=56; h=71;
        imgMuerte3++;
        break;
    case 12:
        pixmap.load(":/ImagenesJuego/Muerte3/12.png");
        w=56; h=71;
        imgMuerte3++;
        break;
    }
}
void personajegraf::muerte4()
{
    stopTimer();
    imagenMuerte4->start(100);
    switch (imgMuerte4) {
    case 1:
        pixmap.load(":/ImagenesJuego/Muerte4/1.png");
        w=67; h=68;
        imgMuerte4++;
        break;
    case 2:
        pixmap.load(":/ImagenesJuego/Muerte4/2.png");
        w=98; h=98;
        imgMuerte4++;
        break;
    case 3:
        pixmap.load(":/ImagenesJuego/Muerte4/3.png");
        w=134; h=121;
        imgMuerte4++;
        break;
    case 4:
        pixmap.load(":/ImagenesJuego/Muerte4/4.png");
        w=134; h=121;
        imgMuerte4++;
        break;
    case 5:
        pixmap.load(":/ImagenesJuego/Muerte4/5.png");
        w=138; h=119;
        imgMuerte4++;
        break;
    case 6:
        pixmap.load(":/ImagenesJuego/Muerte4/6.png");
        w=138; h=119;
        imgMuerte4++;
        break;
    case 7:
        pixmap.load(":/ImagenesJuego/Muerte4/7.png");
        w=122; h=117;
        imgMuerte4++;
        break;
    case 8:
        pixmap.load(":/ImagenesJuego/Muerte4/8.png");
        w=122; h=117;
        imgMuerte4++;
        break;
    case 9:
        pixmap.load(":/ImagenesJuego/Muerte4/9.png");
        w=97; h=102;
        imgMuerte4++;
        break;
    case 10:
        pixmap.load(":/ImagenesJuego/Muerte4/10.png");
        w=97; h=102;
        imgMuerte4++;
        break;
    case 11:
        pixmap.load(":/ImagenesJuego/Muerte4/11.png");
        w=56; h=71;
        imgMuerte4++;
        break;
    case 12:
        pixmap.load(":/ImagenesJuego/Muerte4/12.png");
        w=56; h=71;
        imgMuerte4++;
        break;
    }
}


