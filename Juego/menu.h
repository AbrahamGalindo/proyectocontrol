#ifndef MENU_H
#define MENU_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QMessageBox>

#include "juegoppal.h"
#include "seleccion.h"
#include "codigoqr.h"
#include "instrucciones.h"
#include "enemigos.h"

namespace Ui {
class Menu;
}

class Menu : public QMainWindow
{
    Q_OBJECT

public:
    explicit Menu(QWidget *parent = nullptr);
    ~Menu();
    JuegoPPAL *iniciar;


private slots:
    void on_UnJugador_clicked();

    void on_Multijugador_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_actionEnemigos_triggered();

    void on_actionInstrucciones_triggered();

private:
    Ui::Menu *ui;
    Seleccion *selec;
    QMediaPlayer* click;
    QMediaPlayer* click2;
    QMediaPlayer* click3;

};

#endif // MENU_H
