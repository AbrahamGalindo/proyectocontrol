#ifndef INSTRUCCIONES_H
#define INSTRUCCIONES_H

#include <QDialog>

namespace Ui {
class instrucciones;
}

class instrucciones : public QDialog
{
    Q_OBJECT

public:
    explicit instrucciones(QWidget *parent = nullptr);
    ~instrucciones();
    int cont=1;

private slots:
    void on_pushButton_clicked();

private:
    Ui::instrucciones *ui;
};

#endif // INSTRUCCIONES_H
