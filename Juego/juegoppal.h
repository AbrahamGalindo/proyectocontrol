#ifndef JUEGOPPAL_H
#define JUEGOPPAL_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QTimer>
#include <time.h>
#include <QFile>
#include <QKeyEvent>
#include <QPixmap>
#include <QDebug>
#include <QApplication>
#include <QMainWindow>
#include <QKeyEvent>
#include <fstream>
#include <string>
#include <iostream>
#include <QMessageBox>
#include <QFileDialog>
#include <QMediaPlayer>

#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
using namespace std;


#include "obstaculograf.h"
#include "personajegraf.h"
#include "resultados.h"


#define dt 0.02
namespace Ui {
class JuegoPPAL;
}

class JuegoPPAL : public QMainWindow
{
    Q_OBJECT

public:
    explicit JuegoPPAL(QWidget *parent = nullptr);
    //destrucctor
    ~JuegoPPAL();

    //metodos de simualcion
    void iniciarerson();
    void reiniciar();
    void reiniciarMultijugador();
    void calcularPuntaje();

    //metodos get-set
    void setDosjugadores(bool value);


   //modos de juego
    void unjugador();
    void multijugador();
    void dificultad();





    //quitar-borrar elementos
    void quitarelementos();
    void borrarelementos();


    bool cargar;
    bool Multiplayer=false;
    bool jugador2;

    QList<float> tort;
    QList<float> raseng;
    QList<float> ray;
    QList<float> mon;
    QList<float> vid;
    QList<float> enemi;


    float posX=0;
    float posY=0;

    //cuenta LCD
    int contadorVidas;
    int distanciaLCD;
    int MonedasLCD;

    QSerialPort serial;



private slots:
    void keyPressEvent(QKeyEvent* event);

    void on_actionDetener_triggered();
    void on_actionIniciar_triggered();
    void on_actionReincicar_triggered();

    void actualizar();
    void actualizar2();
    void esperar(void);
    void parar(void);
    void ganador(void);
    void ocultar(void);

    //Obstaculos random
    void TortugasRandom(void);
    void RasenganRandom(void);
    void RayosRandom(void);
    void EnemigoFinalRandom(void);

    void VidaRandom(void);
    void MonedasRandom(void);

    void Chok1(void);
    void Chok2(void);
    void Retro(void);

    void on_Vida_overflow();
    void on_Distancia_overflow();
    void on_Monedas_overflow();
    void on_actionRegresar_al_menu_triggered();
    void on_actionGuardar_triggered();

    void on_actionBluetooth_triggered();

private:
    Ui::JuegoPPAL *ui;

    QTimer *timer;
    QTimer *timer2;

    //Tiempo objetos
    QTimer *TiempoTortuga;
    QTimer *TiempoRasengan;
    QTimer *TiempoRayo;
    QTimer *TiempoEnemigoFinal;

    QTimer *TiempoVida;
    QTimer *TiempoMonedas;

    QTimer *Choque1;
    QTimer *Choque2;
    QTimer *Perdio;
    QTimer *gano;






    QGraphicsScene *scene;
    QLine *linea;

    //listas de objetos
    QList<obstaculoGraf*> tortuga;
    QList<obstaculoGraf*> rasengan;
    QList<obstaculoGraf*> rayos;
    QList<obstaculoGraf*> enemigofinal;

    QList<obstaculoGraf*> vida;
    QList<obstaculoGraf*> monedas;



    personajegraf *personaje;

    //Control numero de objetos
    int numerotortugas;
    int numerorasengan;
    int numerorayos;
    int numeroenemigofinal;

    int numeroVidas;
    int numeromonedas;

    //multijugador
    bool dosjugadores,ban=false,ban1=false;
    int puntaje1=0,puntaje2=0;




    int valor=550;
    int velocidadNivel1=200;

    void colision(personajegraf* a);
    void focus();
    void ponerValores();


    resultados *resultadoP;

    QMediaPlayer *player;
    QMediaPlayer *JuegoPPLMusica;
    QMediaPlayer *Rasengan;
    QMediaPlayer *rayoMusica;
    QMediaPlayer *Golpe;
    QMediaPlayer *Lose;
    QMediaPlayer *clickPPAL;
    QMediaPlayer *clickPPAL2;
    QMediaPlayer *clickPPAL3;
    QMediaPlayer *win2;
    QMediaPlayer *explosion;
    QMediaPlayer *MonedasMusic;
    QMediaPlayer *VidaMusica;
    QMediaPlayer *Nivel2;
    QMediaPlayer *nivel3;

    char data;
    int l=0;
     //Valores iniciales cargar partida

};

#endif // JUEGOPPAL_H
