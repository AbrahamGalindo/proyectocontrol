#include "obstaculograf.h"

obstaculoGraf::obstaculoGraf(float x, float y)
{
    setPos(x,y);
    obstaculo=new obstaculoFisico(x,y);

    //valor imagenes
    imgTortuga=1;
    imgRasengan=1;
    imgRayos=1;
    imgEnemigoFinal=1;

    imgVida=1;
    imgMoneda=1;


    timer= new QTimer();
    timer->start(150);

}
//Destrucctor
obstaculoGraf::~obstaculoGraf()
{
    delete obstaculo;
    delete timer;


}

//Metodo get-set
obstaculoFisico *obstaculoGraf::getItem()
{
    return  obstaculo;
}

//Metodo de simulacion
QRectF obstaculoGraf::boundingRect() const
{
    return QRect(0,0,w,h);
}




//Coneccion timer con slots
void obstaculoGraf::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
   painter->drawPixmap(boundingRect(),pixmap,pixmap.rect());
}

void obstaculoGraf::moverTortuga()
{
    connect(timer,&QTimer::timeout,this,&obstaculoGraf::tortuga);
}


void obstaculoGraf::moverRasengan()
{
    connect(timer,&QTimer::timeout,this,&obstaculoGraf::girar);
}

void obstaculoGraf::moverRayos()
{
    connect(timer,&QTimer::timeout,this,&obstaculoGraf::rayo);
}

void obstaculoGraf::moverEnemigoFinal(){
    connect(timer,&QTimer::timeout,this,&obstaculoGraf::enemigofinal);
}

void obstaculoGraf::moverVida()
{
    connect(timer,&QTimer::timeout,this,&obstaculoGraf::vida);
}

void obstaculoGraf::moverMoneda()
{
    connect(timer,&QTimer::timeout,this,&obstaculoGraf::moneda);
}



//Animacion de los elementos en pantalla
void obstaculoGraf::tortuga()
{
    switch (imgTortuga) {
    case 1:
        pixmap.load(":/ImagenesJuego/Tortuga/T1.png");
        imgTortuga++;
        w=90; h=65;
        break;
    case 2:
        pixmap.load(":/ImagenesJuego/Tortuga/T2.png");
        imgTortuga++;
        w=90 ;h=66;
        break;
    case 3:
        pixmap.load(":/ImagenesJuego/Tortuga/T3.png");
        imgTortuga++;
        w=90 ;h=66;
        break;
    case 4:
        pixmap.load(":/ImagenesJuego/Tortuga/T4.png");
        imgTortuga=1;
        w=90 ;h=66;
        break;

    }
}

void obstaculoGraf::girar()
{
    switch (imgRasengan) {
    case 1:
        pixmap.load(":/ImagenesJuego/Rasengan/rasengan1.png");
        imgRasengan++;
        w=40; h=49;
        break;
    case 2:
        pixmap.load(":/ImagenesJuego/Rasengan/rasengan2.png");
        imgRasengan++;
        w=49; h=41;
        break;
    case 3:
        pixmap.load(":/ImagenesJuego/Rasengan/rasengan3.png");
        imgRasengan++;
        w=38; h=48;
        break;
    case 4:
        pixmap.load(":/ImagenesJuego/Rasengan/rasengan4.png");
        imgRasengan=1;
        w=49; h=37;
        break;
    }
}


void obstaculoGraf::rayo()
{
    switch (imgRayos) {
    case 1:
        pixmap.load(":/ImagenesJuego/Rayo/Rayo1.png");
        imgRayos++;
        w=50;h=200;
        break;
    case 2:
        pixmap.load(":/ImagenesJuego/Rayo/Rayo2.png");
        imgRayos++;
        w=50;h=200;
        break;
    case 3:
        pixmap.load(":/ImagenesJuego/Rayo/Rayo3.png");
        imgRayos++;
        w=50;h=200;
        break;
    case 4:
        pixmap.load(":/ImagenesJuego/Rayo/Rayo4.png");
        imgRayos++;
        w=50;h=200;
        break;
    case 5:
        pixmap.load(":/ImagenesJuego/Rayo/Rayo5.png");
        imgRayos++;
        w=50;h=200;
        break;
    case 6:
        pixmap.load(":/ImagenesJuego/Rayo/Rayo6.png");
        imgRayos=1;
        w=50;h=200;
        break;

    }
}

void obstaculoGraf::enemigofinal()
{
    //w: largo, h: ancho

    switch (imgEnemigoFinal) {
    case 1:
        pixmap.load(":/ImagenesJuego/Enemigo/1.png");
        imgEnemigoFinal++;
        w=191;h=115;
        break;
    case 2:
        pixmap.load(":/ImagenesJuego/Enemigo/2.png");
        imgEnemigoFinal++;
        w=185;h=115;
        break;
    case 3:
        pixmap.load(":/ImagenesJuego/Enemigo/3.png");
        imgEnemigoFinal++;
        w=184;h=115;
        break;
    case 4:
        pixmap.load(":/ImagenesJuego/Enemigo/4.png");
        imgEnemigoFinal++;
        w=184;h=115;
        break;
    case 5:
        pixmap.load(":/ImagenesJuego/Enemigo/5.png");
        imgEnemigoFinal++;
        w=191;h=115;
        break;
    case 6:
        pixmap.load(":/ImagenesJuego/Enemigo/6.png");
        imgEnemigoFinal++;
         w=185;h=115;
        break;
    case 7:
        pixmap.load(":/ImagenesJuego/Enemigo/7.png");
        imgEnemigoFinal++;
         w=184;h=115;
        break;
    case 8:
        pixmap.load(":/ImagenesJuego/Enemigo/8.png");
        imgEnemigoFinal++;
         w=185;h=115;
        break;
    case 9:
        pixmap.load(":/ImagenesJuego/Enemigo/9.png");
        imgEnemigoFinal++;
        w=191;h=115;
        break;
    case 10:
        pixmap.load(":/ImagenesJuego/Enemigo/10.png");
        imgEnemigoFinal++;
        w=197;h=115;
        break;
    case 11:
        pixmap.load(":/ImagenesJuego/Enemigo/11.png");
        imgEnemigoFinal++;
        w=326;h=115;
        break;
    case 12:
        pixmap.load(":/ImagenesJuego/Enemigo/12.png");
        imgEnemigoFinal++;
        w=286;h=115;
        break;
    case 13:
        pixmap.load(":/ImagenesJuego/Enemigo/13.png");
        imgEnemigoFinal++;
        w=185;h=115;
        break;
    case 14:
        pixmap.load(":/ImagenesJuego/Enemigo/14.png");
        imgEnemigoFinal++;
        w=184;h=115;
        break;
    case 15:
        pixmap.load(":/ImagenesJuego/Enemigo/15.png");
        imgEnemigoFinal++;
        w=191;h=115;
        break;
    case 16:
        pixmap.load(":/ImagenesJuego/Enemigo/16.png");
        imgEnemigoFinal++;
        w=225;h=115;
        break;
    case 17:
        pixmap.load(":/ImagenesJuego/Enemigo/17.png");
        imgEnemigoFinal++;
        w=353,h=115;
        break;
    case 18:
        pixmap.load(":/ImagenesJuego/Enemigo/18.png");
        imgEnemigoFinal++;
        w=314;h=115;
        break;
    case 19:
        pixmap.load(":/ImagenesJuego/Enemigo/18.png");
        imgEnemigoFinal++;
        w=188;h=115;
        break;
    case 20:
        pixmap.load(":/ImagenesJuego/Enemigo/20.png");
        imgEnemigoFinal=1;
        w=184;h=115;
        break;
    }
}


void obstaculoGraf::vida()
{
    switch (imgVida) {
    case 1:
        pixmap.load(":/ImagenesJuego/Vida/V1.png");
        imgVida++;
        w=50; h=40;
        break;
    case 2:
        pixmap.load(":/ImagenesJuego/Vida/V2.png");
        imgVida++;
        w=50; h=40;
        break;
    case 3:
        pixmap.load(":/ImagenesJuego/Vida/V3.png");
        imgVida++;
        w=50; h=40;
        break;
    case 4:
        pixmap.load(":/ImagenesJuego/Vida/V4.png");
        imgVida=1;
        w=50; h=40;
        break;
    }
}

void obstaculoGraf::moneda()
{
    switch (imgMoneda) {
    case 1:
        pixmap.load(":/ImagenesJuego/Extras/M1.png");
        imgMoneda++;
        w=40;h=40;
        break;
    case 2:
        pixmap.load(":/ImagenesJuego/Extras/M2.png");
        imgMoneda++;
        w=26;h=40;
        break;
    case 3:
        pixmap.load(":/ImagenesJuego/Extras/M3.png");
        imgMoneda++;
        w=10;h=40;
        break;
    case 4:
        pixmap.load(":/ImagenesJuego/Extras/M4.png");
        imgMoneda=1;
        w=26;h=40;
        break;

    }
}


//Slots
void obstaculoGraf::actualizar(float dt)
{
    obstaculo->actualizar(dt);
    setPos(obstaculo->getPx(),obstaculo->getPy());
}

void obstaculoGraf::actualizarMoneda(float dt)
{
    obstaculo->ActualizarMoneda(dt);
    setPos(obstaculo->getPx(),obstaculo->getPy());
}
